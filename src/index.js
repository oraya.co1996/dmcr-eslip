
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
const axios = require('axios');

window.$baseUrl = 'http://api.dmcr.go.th:3500/service-api/';

if(localStorage.getItem("token")) {
    var diff =(new Date().getTime() - parseInt(localStorage.getItem("token_time"))) / 1000;
    diff /= 60;
    if(Math.abs(Math.round(diff)) >= 10080) {
        let body = {
            apptoken : "dmcr-eslip-2564-api"
        }
        axios.post(window.$baseUrl + "auth", body)
        .then(function (res) {
            console.log(res);
            localStorage.setItem("token", res.data.tokenid);
            localStorage.setItem("token_time", new Date().getTime());
            window.$header = { headers: { 'Authorization': 'Bearer ' + localStorage.getItem("token") }}
        }).catch(function (error) {
            console.log(error);
        });
    } else {
        window.$header = { headers: { 'Authorization': 'Bearer ' + localStorage.getItem("token") }}
    }
} else {
    let body = {
        apptoken : "dmcr-eslip-2564-api"
    }
    axios.post(window.$baseUrl + "auth", body)
    .then(function (res) {
        console.log(res);
        localStorage.setItem("token", res.data.tokenid);
        localStorage.setItem("token_time", new Date().getTime());
        window.$header = { headers: { 'Authorization': 'Bearer ' + localStorage.getItem("token") }}
    }).catch(function (error) {
        console.log(error);
    });
}


ReactDOM.render(<App />, document.getElementById('root'));
