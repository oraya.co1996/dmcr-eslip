
import React from 'react';

function Footerin() {
    return (
        <React.Fragment>
<footer class="site-footer -site-member">
	<div class="main-footer">
		<div class="footer-address">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="address" data-aos="fade-right">
							<figure class="brand">
								<img class="img-logo-ft" src="img/static/brand.png" alt="dmcr e-slip กรมทรัพยากรทางทะเลและชายฝั่ง" />
							</figure>
							<div class="title">
								<h2 class="title-footer">DMCR E-SLIP</h2>	
								<p class="subtitle-footer">กระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม</p>
							</div>
						</div>
					</div>
				<div class="col-lg-6">
					<div class="address contact" data-aos="fade-left">
						<div class="address-info">
							<div class="row no-gutters">
								<div class="col-auto">
									<div class="thumb">
										<div class="cover">
											<img class="img-cover" src="img/icon/location-icon.svg" alt="location icon" />
										</div>
									</div>
								</div>
								<div class="col">
									<div class="inner">เลขที่ 120 หมู่ ที่ 3 ชั้น ที่ 5-9 อาคาร รัฐประศาสนภักดี ศูนย์ราชการเฉลิมพระเกียรติ 80 พรรษา 5 ธันวาคม 2550 ถนน แจ้งวัฒนะ แขวง ทุ่งสองห้อง เขต หลักสี่ กรุงเทพมหานคร 10210</div>
								</div>
							</div>
							<div class="row no-gutters pt-address-ft">
								<div class="col-md-4 mb-footer">
									<a class="link" href="tel:+66 2141 1300">
										<div div class="row no-gutters">
											<div class="col-auto">
												<div class="thumb">
													<div class="cover">
														<img class="img-cover" src="img/icon/phone.svg" alt="phone" />
													</div>
												</div>
											</div>
											<div class="col d-flex align-items-center">
												<div class="inner">
													<p class="text">
														โทรศัพท์<br />
														+66 2141 1300
													</p>
												</div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-4 mb-footer">
									<a class="link" style={{cursor: 'unset'}}>
										<div div class="row no-gutters">
											<div class="col-auto">
												<div class="thumb">
													<div class="cover">
														<img class="img-cover" src="img/icon/fax.svg" alt="fax" />
													</div>
												</div>
											</div>
											<div class="col d-flex align-items-center">
												<div class="inner">
													<p class="text fax">
														แฟกซ์ <br />
														+66 2143 9242
													</p>
												</div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-4 mb-footer">
									<a class="link" href="mailto:it@dmcr.mail.go.th">
										<div div class="row no-gutters">
											<div class="col-auto">
												<div class="thumb">
													<div class="cover">
														<img class="img-cover" src="img/icon/mail.svg" alt="mail" />
													</div>
												</div>
											</div>
											<div class="col d-flex align-items-center">
												<div class="inner">
													<p class="text">
													อีเมล<br />
													it@dmcr.mail.go.th
													</p>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		<div class="footer-info">
			<div class="container">
				<div class="row no-gutters pt-footer-info">
					<div class="col-lg-9 col-md-8" data-aos="fade-right" data-aos-delay="200">
						<div class="wrapper">
							<div class="desc">
								สงวนลิขสิทธิ์ © พ.ศ.2556 กรมทรัพยากรทางทะเลและชายฝั่ง พัฒนาระบบโดย กองสารสนเทศและเทคโนโลยีการสำรวจทรัพยากรทางทะเลและชายฝั่ง ห้ามนำส่วนหนึ่ง
								ส่วนใดในเว็บไซต์นี้ไปทำซ้ำหรือ เผยแพร่ในรูปแบบใดๆ หรือวิธีอื่นใด ยกเว้นเพื่อวัตถุประสงค์ทางการศึกษาหากมีความประสงค์ใช้ข้อมูล
								ตัวเลขหรือข้อมูลเชิงพื้นที่ในการอ้างอิงโปรดสอบทานความถูกต้องกับหน่วยงานโดยตรง เริ่มใช้งานตั้งแต่ กรกฏาคม พ.ศ.2556
							</div>
						</div>
						<div class="policy-footer">
							<div class="row align-items-center w-policy">
								<div class="col">
									<ul class="policy-list item-list" id="get_menu_policy">
										<li class="item"><a href="javascript:void(0)" data-url="home/policy" data-page="92" class="link policyApi" title="นโยบายการรักษาความมั่นคงปลอดภัย">นโยบายการคุ้มครองข้อมูลส่วนบุคคล</a></li>
										<li class="item"><a href="javascript:void(0)" data-url="home/policy" data-page="93" class="link policyApi" title="นโยบายเว็บไซต์">นโยบายเว็บไซต์</a></li>
										<li class="item"><a href="javascript:void(0)" data-url="home/policy" data-page="94" class="link policyApi" title="นโยบายการคุ้มครองข้อมูลส่วนบุคคล">นโยบายการรักษาความมั่นคงปลอดภัยเว็บไซต์</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>	
					<div class="col-lg-3 col-md-4" data-aos="fade-left" data-aos-delay="200">
						<div class="standard">
							<div class="row row-10">
								<div class="col-auto col-md-4">
									<div class="ipv6">
										<a href="http://ipv6-test.com/validate.php?url=referer" title="ipv6 ready'" target="_blank" rel="nofollow">
											<img src="img/icon/button-ipv6.png?v=1001" alt="ipv6 ready" title="ipv6 ready" />
										</a>
									</div>
								</div>
								<div class="col-auto col-md-4">
									<div class="w3c">
										<a href="https://www.w3.org/WAI/WCAG2AAA-Conformance"
										title="Explanation of WCAG 2.0 Level Triple-A Conformance" target="_blank">
										<img height="32" width="88"
											src="https://www.w3.org/WAI/wcag2A"
											alt="Level Triple-A conformance, W3C WAI Web Content Accessibility Guidelines 2.0" />
										</a>
									</div>
								</div>
								<div class="col-auto col-md-4">
									<div class="w3c">
										<a href="http://jigsaw.w3.org/css-validator/check/referer" target="_blank">
											<img style={{ bordertop: '0px', height: '31px', borderright: '0px', width: '88px', borderbottom: '0px', borderleft: '0px'}} alt="Valid CSS!" src="http://jigsaw.w3.org/css-validator/images/vcss?v=20161016" />
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="social">
							<ul class="social-list item-list">
								<li class="item"><a class="link" target="_blank" href="https://www.facebook.com/DMCRTH"><img class="img-social" src="img/icon/social-fb.png" alt="fb" /></a></li>
								<li class="item"><a class="link" target="_blank" href="https://twitter.com/dmcrth"><img class="img-social" src="img/icon/social-tw.png" alt="twitter" /></a></li>
								<li class="item"><a class="link" target="_blank" href="https://www.youtube.com/channel/UC4_fXOSelonvCzuv0jc4qbg"><img class="img-social" src="img/icon/social-yt.png" alt="youtube" /></a></li>
								<li class="item"><a class="link" target="_blank" href="https://www.instagram.com/dmcrth/"><img class="img-social" src="img/icon/social-ig.png" alt="IG" /></a></li>
								<li class="item"><a class="link" target="_blank" href="http://line.me/ti/p/~@DMCRTH"><img class="img-social" src="img/icon/social-li.png" alt="line" /></a></li>
								<li class="item"><a class="link" target="_blank" href="http://183.88.224.221/dmcr_dailynews/weadmin/"><img class="img-social" src="img/icon/social-ic2.png" alt="line" /></a></li>
							</ul>
						</div>
						<div class="footer-bar">
							<div class="apDiv1" style={{display: 'inline-block'}}>
							
								{/* <script type="text/javascript">document.write(unescape("%3Cscript src=%27https://s10.histats.com/js15.js%27 type=%27text/javascript%27%3E%3C/script%3E"));</script>
								<a href="https://www.histats.com" target="_blank" title="hit tracker" >
								<script  type="text/javascript">
								try {Histats.start(1,2460058,4,1032,150,25,"00011111");
								Histats.track_hits();} catch(err){};
								</script></a> */}
								<noscript><a href="https://www.histats.com" target="_blank"><img  src="https://sstatic1.histats.com/0.gif?2460058&101" alt="hit tracker" border="0" /></a></noscript>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
        </React.Fragment>
    );
}

export default Footerin;

