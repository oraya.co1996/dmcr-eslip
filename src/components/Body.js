import React from 'react';
import Footer from './Footer';
import Header from './Header';

function Body() {
    return (
        <React.Fragment>
            <Header />
            <section className="site-container">
                <div className="topgraphic" data-aos="fade-zoom-in">
                    <div className="slider">
                        <div className="item">
                            <a href className="lonk">
                                <div className="b-topgraphic">
                                    <figure className="cover">
                                        <img src="img/background/tp-img1.png" alt="tp-img-1" />
                                    </figure>
                                    <div className="container">
                                        <div className="b-text">
                                            <div className="top-title" data-aos="fade-right" data-aos-delay={300}>DMCR</div>
                                            <div className="h-title" data-aos="fade-left" data-aos-delay={600}>E-SLIP</div>
                                            <div className="bottom-title" data-aos="fade-right" data-aos-delay={900}>กรมทรัพยากรทางทะเลและชายฝั่ง</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div className="item">
                            <a href className="lonk">
                                <div className="b-topgraphic">
                                    <figure className="cover">
                                        <img src="img/background/tp-img2.png" alt="tp-img-2" />
                                    </figure>
                                    <div className="container">
                                        <div className="b-text">
                                            <div className="top-title" data-aos="fade-right" data-aos-delay={300}>DMCR</div>
                                            <div className="h-title" data-aos="fade-left" data-aos-delay={600}>E-SLIP</div>
                                            <div className="bottom-title" data-aos="fade-right" data-aos-delay={900}>กรมทรัพยากรทางทะเลและชายฝั่ง</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                {/* news */}
                <div className="wg-news" data-aos="fade-up">
                    <div data-aos="zoom-in-left" data-aos-delay={300}>
                        <div className="mn mn-lg"><img src="img/background/w810-lg.png" alt="" /></div>
                    </div>
                    <div data-aos="zoom-in-right">
                        <div className="mn mn-sm"><img src="img/background/w810-sm.png" alt="" /></div>
                    </div>
                    <div className="container">
                        <div className="h-title text-light">ข่าวสาร &amp; กิจกรรม</div>
                        <div className="b-hex">
                            <ul className="hexGrid">
                                <li className="hex" data-aos="flip-left" data-aos-delay={300}>
                                    <div className="hexIn">
                                        <a className="hexLink" href="/news">
                                            <div className="img" style={{ backgroundImage: 'url(img/background/pic.jpeg)' }} />
                                            <div className="date">23-03-2021</div>
                                            <div className="title text-limit">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''''s standard dummy</div>
                                            <div className="desc text-limit">ontrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece
                                             of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney 
                                             College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the
                                              cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 
                                              of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory
                                               of ethics, very popular during the Renaissance. The first line of Lor</div>
                                        </a>
                                    </div>
                                </li>
                                <li className="hex" data-aos="flip-left" data-aos-delay={300}>
                                    <div className="hexIn">
                                        <a className="hexLink" href="/news">
                                            <div className="img" style={{ backgroundImage: 'url(img/background/57174.jpg)' }} />
                                            <div className="date">20-04-2021</div>
                                            <div className="title text-limit">Test</div>
                                            <div className="desc text-limit">Test</div>
                                        </a>
                                    </div>
                                </li>
                                {/* <li className="hex" data-aos="flip-left" data-aos-delay={300}>
                                    <div className="hexIn">
                                        <a className="hexLink" href="/news">
                                            <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-3.png)' }} />
                                            <div className="date">31 มีนาคม 2564</div>
                                            <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                            <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                        </a>
                                    </div>
                                </li> */}
                                {/* <li className="hex" data-aos="flip-left" data-aos-delay={300}>
                                    <div className="hexIn">
                                        <a className="hexLink" href="/news">
                                            <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-4.png)' }} />
                                            <div className="date">31 มีนาคม 2564</div>
                                            <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                            <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                        </a>
                                    </div>
                                </li>
                                <li className="hex" data-aos="flip-left" data-aos-delay={300}>
                                    <div className="hexIn">
                                        <a className="hexLink" href="/news">
                                            <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-5.png)' }} />
                                            <div className="date">31 มีนาคม 2564</div>
                                            <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                            <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                        </a>
                                    </div>
                                </li> */}
                            </ul>
                        </div>
                        <div className="action text-center" data-aos="fade-up">
                            <a href="news.php" className="btn btn-secondary btn-round-5 btn-large">ดูทั้งหมด</a>
                        </div>
                    </div>
                </div>
                {/* news */}
                {/* login */}
                <div className="wg-login">
                    <div className="container">
                        <div className="row no-gutters">
                            <div className="col-md-6 order-2 order-md-1" data-aos="fade-up-right">
                                <div className="cover">
                                    <div className="b-img-1">
                                        <img src="img/background/money-1.png" alt="" />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 order-1 order-md-2 height" data-aos="fade-left">
                                <div className="box-text">
                                    <div className="h-title text-light">ลงชื่อเข้าใช้</div>
                                    <div className="desc text-light">ลงชื่อเข้าใช้งาน DMCR SLIP กรมทรัพยากรทางทะเล<br />และชายฝั่ง กระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม</div>
                                    <div className="action">
                                        <a href="/check-idno" className="btn btn-primary btn-round-5 btn-large border-0">เข้าสู่ระบบ</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* login */}
                {/* about */}
                <div className="wg-about">
                    <div className="container">
                        <div className="h-title text-primary text-center" data-aos="fade-up">เกี่ยวกับโครงการ</div>
                        <div className="wg-about-list" data-aos="fade-up">
                            <div className="slider">
                                <div className="item">
                                    <div className="wrapper">
                                        <a href="/about" className="link">
                                            <div className="cover">
                                                <div className="b-img">
                                                    <img src="img/static/ab-1.png" alt="ab-1" />
                                                </div>
                                            </div>
                                            <div className="content">
                                                <div className="title text-primary">test1</div>
                                                <div className="desc"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div className="item">
                                    <div className="wrapper">
                                        <a href="/about" className="link">
                                            <div className="cover">
                                                <div className="b-img">
                                                    <img src="img/static/ab-2.png" alt="ab-2" />
                                                </div>
                                            </div>
                                            <div className="content">
                                                <div className="title text-primary">test</div>
                                                <div className="desc"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                {/* <div className="item">
                                    <div className="wrapper">
                                        <a href="/about" className="link">
                                            <div className="cover">
                                                <div className="b-img">
                                                    <img src="img/static/ab-3.png" alt="ab-3" />
                                                </div>
                                            </div>
                                            <div className="content">
                                                <div className="title text-primary">นโยบาย</div>
                                                <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                            </div>
                                        </a>
                                    </div>
                                </div> */}
                                {/* <div className="item">
                                    <div className="wrapper">
                                        <a href="/about" className="link">
                                            <div className="cover">
                                                <div className="b-img">
                                                    <img src="img/static/ab-1.png" alt="ab-1" />
                                                </div>
                                            </div>
                                            <div className="content">
                                                <div className="title text-primary">กฎระเบียบ</div>
                                                <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                            </div>
                                        </a>
                                    </div>
                                </div> */}
                                {/* <div className="item">
                                    <div className="wrapper">
                                        <a href="/about" className="link">
                                            <div className="cover">
                                                <div className="b-img">
                                                    <img src="img/static/ab-2.png" alt="ab-2" />
                                                </div>
                                            </div>
                                            <div className="content">
                                                <div className="title text-primary">ที่มาโครงการ</div>
                                                <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                            </div>
                                        </a>
                                    </div>
                                </div> */}
                                {/* <div className="item">
                                    <div className="wrapper">
                                        <a href="/about" className="link">
                                            <div className="cover">
                                                <div className="b-img">
                                                    <img src="img/static/ab-3.png" alt="ab-3" />
                                                </div>
                                            </div>
                                            <div className="content">
                                                <div className="title text-primary">นโยบาย</div>
                                                <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                            </div>
                                        </a>
                                    </div>
                                </div> */}
                                {/* <div className="item">
                                    <div className="wrapper">
                                        <a href="/about" className="link">
                                            <div className="cover">
                                                <div className="b-img">
                                                    <img src="img/static/ab-1.png" alt="ab-1" />
                                                </div>
                                            </div>
                                            <div className="content">
                                                <div className="title text-primary">กฎระเบียบ</div>
                                                <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                            </div>
                                        </a>
                                    </div>
                                </div> */}
                                {/* <div className="item">
                                    <div className="wrapper">
                                        <a href="/about" className="link">
                                            <div className="cover">
                                                <div className="b-img">
                                                    <img src="img/static/ab-2.png" alt="ab-2" />
                                                </div>
                                            </div>
                                            <div className="content">
                                                <div className="title text-primary">ที่มาโครงการ</div>
                                                <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                            </div>
                                        </a>
                                    </div>
                                </div> */}
                                {/* <div className="item">
                                    <div className="wrapper">
                                        <a href="/about" className="link">
                                            <div className="cover">
                                                <div className="b-img">
                                                    <img src="img/static/ab-3.png" alt="ab-3" />
                                                </div>
                                            </div>
                                            <div className="content">
                                                <div className="title text-primary">นโยบาย</div>
                                                <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                            </div>
                                        </a>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
                {/* about */}
            </section>
            <Footer />
        </React.Fragment>
    );
}

export default Body;