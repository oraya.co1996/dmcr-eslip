
import React from 'react';

function Footer() {
    return (
        <React.Fragment>
            <footer className="site-footer">
                <div className="main-footer">
                    <div className="footer-address">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="address" data-aos="fade-right">
                                        <figure className="brand">
                                            <img className="img-logo-ft" src="img/static/brand.png" alt="dmcr e-slip กรมทรัพยากรทางทะเลและชายฝั่ง" />
                                        </figure>
                                        <div className="title">
                                            <h2 className="title-footer">DMCR E-SLIP</h2>
                                            <p className="subtitle-footer">กระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="address contact" data-aos="fade-left">
                                        <div className="address-info">
                                            <div className="row no-gutters">
                                                <div className="col-auto">
                                                    <div className="thumb">
                                                        <div className="cover">
                                                            <img className="img-cover" src="img/icon/location-icon.svg" alt="location icon" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col">
                                                    <div className="inner">เลขที่ 120 หมู่ ที่ 3 ชั้น ที่ 5-9 อาคาร รัฐประศาสนภักดี ศูนย์ราชการเฉลิมพระเกียรติ 80 พรรษา 5 ธันวาคม 2550 ถนน แจ้งวัฒนะ แขวง ทุ่งสองห้อง เขต หลักสี่ กรุงเทพมหานคร 10210</div>
                                                </div>
                                            </div>
                                            <div className="row no-gutters pt-address-ft">
                                                <div className="col-md-4 mb-footer">
                                                    <a className="link" href="tel:+66 2141 1300">
                                                        <div div className="row no-gutters">
                                                            <div className="col-auto">
                                                                <div className="thumb">
                                                                    <div className="cover">
                                                                        <img className="img-cover" src="img/icon/phone.svg" alt="phone" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col d-flex align-items-center">
                                                                <div className="inner">
                                                                    <p className="text">
                                                                        โทรศัพท์<br />
                                                                        +66 2141 1300
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div className="col-md-4 mb-footer">
                                                    <a className="link" style={{ cursor: 'unset' }}>
                                                        <div div className="row no-gutters">
                                                            <div className="col-auto">
                                                                <div className="thumb">
                                                                    <div className="cover">
                                                                        <img className="img-cover" src="img/icon/fax.svg" alt="fax" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col d-flex align-items-center">
                                                                <div className="inner">
                                                                    <p className="text fax">
                                                                        แฟกซ์ <br />
                                                                        +66 2143 9242
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div className="col-md-4 mb-footer">
                                                    <a className="link" href="mailto:it@dmcr.mail.go.th">
                                                        <div div className="row no-gutters">
                                                            <div className="col-auto">
                                                                <div className="thumb">
                                                                    <div className="cover">
                                                                        <img className="img-cover" src="img/icon/mail.svg" alt="mail" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col d-flex align-items-center">
                                                                <div className="inner">
                                                                    <p className="text">
                                                                        อีเมล<br />
                                                                        it@dmcr.mail.go.th
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="footer-info">
                        <div className="container">
                            <div className="row no-gutters pt-footer-info">
                                <div className="col-lg-9 col-md-8" data-aos="fade-right" data-aos-delay={200}>
                                    <div className="wrapper">
                                        <div className="desc">
                                            สงวนลิขสิทธิ์ © พ.ศ.2556 กรมทรัพยากรทางทะเลและชายฝั่ง พัฒนาระบบโดย กองสารสนเทศและเทคโนโลยีการสำรวจทรัพยากรทางทะเลและชายฝั่ง ห้ามนำส่วนหนึ่ง
                                            ส่วนใดในเว็บไซต์นี้ไปทำซ้ำหรือ เผยแพร่ในรูปแบบใดๆ หรือวิธีอื่นใด ยกเว้นเพื่อวัตถุประสงค์ทางการศึกษาหากมีความประสงค์ใช้ข้อมูล
                                            ตัวเลขหรือข้อมูลเชิงพื้นที่ในการอ้างอิงโปรดสอบทานความถูกต้องกับหน่วยงานโดยตรง เริ่มใช้งานตั้งแต่ กรกฏาคม พ.ศ.2556
                                        </div>
                                    </div>
                                    <div className="policy-footer">
                                        <div className="row align-items-center w-policy">
                                            <div className="col">
                                                <ul className="policy-list item-list" id="get_menu_policy">
                                                    <li className="item"><a href="javascript:void(0)" data-url="home/policy" data-page={92} className="link policyApi" title="นโยบายการรักษาความมั่นคงปลอดภัย">นโยบายการคุ้มครองข้อมูลส่วนบุคคล</a></li>
                                                    <li className="item"><a href="javascript:void(0)" data-url="home/policy" data-page={93} className="link policyApi" title="นโยบายเว็บไซต์">นโยบายเว็บไซต์</a></li>
                                                    <li className="item"><a href="javascript:void(0)" data-url="home/policy" data-page={94} className="link policyApi" title="นโยบายการคุ้มครองข้อมูลส่วนบุคคล">นโยบายการรักษาความมั่นคงปลอดภัยเว็บไซต์</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-4" data-aos="fade-left" data-aos-delay={200}>
                                    <div className="standard">
                                        <div className="row row-10">
                                            <div className="col-auto col-md-4"> {/* mr-standard */}
                                                <div className="ipv6">
                                                    <a href="http://ipv6-test.com/validate.php?url=referer" title="ipv6 ready'" target="_blank" rel="nofollow">
                                                        <img src="img/icon/button-ipv6.png?v=1001" alt="ipv6 ready" title="ipv6 ready" />
                                                    </a>
                                                </div>
                                            </div>
                                            <div className="col-auto col-md-4">
                                                <div className="w3c">
                                                    <a href="https://www.w3.org/WAI/WCAG2AAA-Conformance" title="Explanation of WCAG 2.0 Level Triple-A Conformance" target="_blank">
                                                        <img src="https://www.w3.org/WAI/wcag2A" alt="Level Triple-A conformance, W3C WAI Web Content Accessibility Guidelines 2.0" width={88} height={32} />
                                                    </a>
                                                </div>
                                            </div>
                                            <div className="col-auto col-md-4">
                                                <div className="w3c">
                                                    <a href="http://jigsaw.w3.org/css-validator/check/referer" target="_blank">
                                                        <img style={{ borderTop: '0px', height: '31px', borderRight: '0px', width: '88px', borderBottom: '0px', borderLeft: '0px' }} alt="Valid CSS!" src="http://jigsaw.w3.org/css-validator/images/vcss?v=20161016" />
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="social">
                                        <ul className="social-list item-list">
                                            <li className="item"><a className="link" target="_blank" href="https://www.facebook.com/DMCRTH"><img className="img-social" src="img/icon/social-fb.png" alt="fb" /></a></li>
                                            <li className="item"><a className="link" target="_blank" href="https://twitter.com/dmcrth"><img className="img-social" src="img/icon/social-tw.png" alt="twitter" /></a></li>
                                            <li className="item"><a className="link" target="_blank" href="https://www.youtube.com/channel/UC4_fXOSelonvCzuv0jc4qbg"><img className="img-social" src="img/icon/social-yt.png" alt="youtube" /></a></li>
                                            <li className="item"><a className="link" target="_blank" href="https://www.instagram.com/dmcrth/"><img className="img-social" src="img/icon/social-ig.png" alt="IG" /></a></li>
                                            <li className="item"><a className="link" target="_blank" href="http://line.me/ti/p/~@DMCRTH"><img className="img-social" src="img/icon/social-li.png" alt="line" /></a></li>
                                            <li className="item"><a className="link" target="_blank" href="http://183.88.224.221/dmcr_dailynews/weadmin/"><img className="img-social" src="img/icon/social-ic2.png" alt="line" /></a></li>
                                        </ul>
                                    </div>
                                    <div className="footer-bar">
                                        <div className="apDiv1" style={{ display: 'inline-block' }}>
                                            {/* Histats.com  START  (standard)*/}
                                            <a href="https://www.histats.com" target="_blank" title="hit tracker" />
                                            <noscript>&lt;a href="https://www.histats.com" target="_blank"&gt;&lt;img  src="https://sstatic1.histats.com/0.gif?2460058&amp;101" alt="hit tracker" border="0"&gt;&lt;/a&gt;</noscript>
                                            {/* Histats.com  END  */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </React.Fragment>
    );
}

export default Footer;

