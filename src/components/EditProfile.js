import React, { Component } from "react";
import Footer from "./Footer";
import Headermember from "./Headermember";
const axios = require("axios");

const header = { Authorization: "Bearer " + localStorage.getItem("token") };

let bank = [];
let position = [];
let affiliate = [];

function back() {
  window.location.href = "/profile";
}
function selectImg() {
  document.getElementById("inputImg").click();
}

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        identificationNo: "",
        fname: "",
        lname: "",
        position_id: 0,
        affiliation_id: 0,
        tel: "",
        bank_id: 0,
        major_bank: "",
        name_bank: "",
        number_bank: "",
        salary: 0,
        email: "",
        note: "",
        masterkey: "",
        mem_id: 0,
        pic: "",
      },
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getProfile();
    this.getBank();
    this.getPosition();
    this.getAffiliate();
  }

  onChangeUpdate = (e) => {
    const name = e.target.name;
    const value = e.target.type === "file" ? e.target.files[0] : e.target.value;
    this.setState({ value: value });
    this.state.update_text = value;
    switch (name) {
      case "identificationNo":
        this.state.user.identificationNo = this.state.update_text;
        break;
      case "fname":
        this.state.user.fname = this.state.update_text;
        break;
      case "lname":
        this.state.user.lname = this.state.update_text;
        break;
      case "position_id":
        this.state.user.position_id = this.state.update_text;
        break;
      case "affiliation_id":
        this.state.user.affiliation_id = this.state.update_text;
        break;
      case "tel":
        this.state.user.tel = this.state.update_text;
        break;
      case "bank_id":
        this.state.user.bank_id = this.state.update_text;
        break;
      case "major_bank":
        this.state.user.major_bank = this.state.update_text;
        break;
      case "name_bank":
        this.state.user.name_bank = this.state.update_text;
        break;
      case "number_bank":
        this.state.user.number_bank = this.state.update_text;
        break;
      case "salary":
        this.state.user.salary = this.state.update_text;
        break;
      case "email":
        this.state.user.email = this.state.update_text;
        break;
      case "note":
        this.state.user.note = this.state.update_text;
        break;
      case "masterkey":
        this.state.user.masterkey = this.state.update_text;
        break;
      case "mem_id":
        this.state.user.mem_id = this.state.update_text;
        break;
      case "inputImg":
        this.state.pic = this.state.update_text;
        var file = this.state.update_text;
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);
        reader.onloadend = function (e) {
          this.setState({ imgSrc: [reader.result] });
        }.bind(this);
        break;
      default:
    }
  };

  getProfile() {
    let currentComponent = this;
    let config = {
      method: "get",
      url:
        window.$baseUrl +
        "eslip/user?method=infoProfile&mem_id=" +
        localStorage.getItem("memberID") +
        "&masterkey=official",
      headers: header,
    };
    axios(config)
      .then(function (res) {
        console.log(res);
        if (res.data.code === 1001) {
          currentComponent.setState({ user: res.data.user_info });
        } else {
          alert(res.data.code + ": " + res.data.msg);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  getBank() {
    let config = {
      method: "get",
      url: window.$baseUrl + "eslip/user?method=get_bank",
      headers: header,
    };
    axios(config)
      .then(function (res) {
        console.log(res);
        if (res.data.code === 1001) {
          bank = res.data.data;
          console.log("bank", bank);
        } else {
          alert(res.data.code + ": " + res.data.msg);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  getPosition() {
    let config = {
      method: "get",
      url:
        window.$baseUrl +
        "eslip/user?method=position_affiliation&masterkey=position",
      headers: header,
    };
    axios(config)
      .then(function (res) {
        console.log(res);
        if (res.data.code === 1001) {
          position = res.data.data;
          console.log("position", position);
        } else {
          alert(res.data.code + ": " + res.data.msg);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  getAffiliate() {
    let config = {
      method: "get",
      url:
        window.$baseUrl +
        "eslip/user?method=position_affiliation&masterkey=affiliate",
      headers: header,
    };
    axios(config)
      .then(function (res) {
        console.log(res);
        if (res.data.code === 1001) {
          affiliate = res.data.data;
          console.log("affiliate", affiliate);
        } else {
          alert(res.data.code + ": " + res.data.msg);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  handleSubmit(event) {
    let header = {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    };
    console.log(header, this.state);
    event.preventDefault();
    let formData = new FormData();
    formData.append("method", "update_profile");
    formData.append("mem_id", localStorage.getItem("memberID"));
    formData.append("masterkey", this.state.user.masterkey);
    formData.append("identificationNo", this.state.user.identificationNo);
    formData.append("fname", this.state.user.fname);
    formData.append("lname", this.state.user.lname);
    formData.append("position_id", this.state.user.position_id);
    formData.append("affiliate_id", this.state.user.affiliation_id);
    formData.append("tel", this.state.user.tel);
    formData.append("bank_id", this.state.user.bank_id);
    formData.append("major_bank", this.state.user.major_bank);
    formData.append("name_bank", this.state.user.name_bank);
    formData.append("number_bank", this.state.user.number_bank);
    formData.append("salary", this.state.user.salary);
    formData.append("email", this.state.user.email);
    formData.append("note", this.state.user.note);
    formData.append("image_user", this.state.pic);
    console.log(
      formData.get("method"),
      formData.get("mem_id"),
      formData.get("masterkey"),
      formData.get("identificationNo"),
      formData.get("fname"),
      formData.get("lname"),
      formData.get("position_id"),
      formData.get("affiliate_id"),
      formData.get("tel"),
      formData.get("bank_id"),
      formData.get("major_bank"),
      formData.get("name_bank"),
      formData.get("number_bank"),
      formData.get("salary"),
      formData.get("email"),
      formData.get("note"),
      formData.get("image_user")
    );
    var config = {
      method: "put",
      url: window.$baseUrl + "eslip/user",
      headers: header,
      data: formData,
    };
    axios(config)
      .then(function (res) {
        console.log(res);
        if (res.data.code === 1001) {
          window.location.href = "/profile";
        } else {
          alert(res.data.code + ": " + res.data.msg);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        <Headermember />
        <section class="site-container -site-member-page -site-member-content">
          <div class="bg-slide">
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide01.jpg" alt="" class="lazy" />
              </figure>
            </div>
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide02.jpg" alt="" class="lazy" />
              </figure>
            </div>
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide03.jpg" alt="" class="lazy" />
              </figure>
            </div>
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide04.jpg" alt="" class="lazy" />
              </figure>
            </div>
          </div>

          <div class="-site-member-inner">
            <div class="container">
              <form
                onSubmit={this.handleSubmit}
                class="form-default"
              >
                <div class="member-default">
                  <div class="member-head">
                    <div
                      class="page-tag -profile"
                      data-aos="fade-down"
                      data-aos-delay="300"
                    >
                      <div class="icon">
                        <img
                          src="img/icon/i-mem-nav01.svg"
                          alt="ข้อมูลส่วนตัว"
                          class="lazy"
                        />
                      </div>
                      <div class="txt">ข้อมูลส่วนตัว</div>
                    </div>
                  </div>
                  <div class="member-body">
                    <div class="member-profile">
                      <div class="row row-0">
                        <div class="col-auto">
                          <div class="profile-image">
                            <div
                              class="thumb"
                              data-aos="fade-in"
                              data-aos-delay="900"
                            >
                              <figure class="cover">
                              {!this.state.user.pic && !this.state.imgSrc ? (
                                <img
                                  src="img/upload/user.jpg"
                                  class="lazy"
                                  // style={{ height: "150px", width: "auto", border: "1px solid #ddd" }}
                                  alt=""
                                  onClick={() => selectImg()}
                                />):this.state.user.pic && !this.state.imgSrc ? (
                                <img
                                  src={this.state.user.pic}
                                  class="lazy"
                                  // style={{ minHeight: "150px", minWidth: "150px", border: "1px solid #ddd" }}
                                  alt=""
                                  onClick={() => selectImg()}
                                />
                              ) : this.state.imgSrc ? (
                                <img
                                  src={this.state.imgSrc}
                                  class="lazy"
                                  // style={{ height: "150px", width: "auto", border: "1px solid #ddd" }}
                                  alt=""
                                  onClick={() => selectImg()}
                                />
                              ) : null}
                              <input
                                type="file"
                                style={{ display: "none" }}
                                name="inputImg"
                                id="inputImg"
                                onChange={this.onChangeUpdate}
                              />

                                {/* <img
                                  src="img/upload/user.jpg"
                                  alt="ข้าราชการ ลินดา สุทธิกาณจน์"
                                  class="lazy"
                                /> */}

                              </figure>
                              <div class="ojb-shadow"></div>
                            </div>
                            <div class="content">
                              <div class="name" data-aos="fade-up">
                                {this.state.user.fname} {this.state.user.lname}
                              </div>
                              <div class="career" data-aos="fade-up">
                                ข้าราชการ
                              </div>
                              <div class="job-title" data-aos="fade-up">
                                นักวิชาการเงินและบัญชี
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col">
                          <div
                            class="profile-content -edit"
                            data-aos="fade-in"
                            data-aos-delay="400"
                          >
                            <div class="inner">
                              <div class="row row-40">
                                <div class="col-lg-12">
                                  <div class="form-group">
                                    <label class="control-label">
                                      เลขประจำตัวประชาชน
                                    </label>
                                    <div class="block-control">
                                      <input
                                        type="text"
                                        className="form-control"
                                        name="identificationNo"
                                        value={this.state.user.identificationNo}
                                        onChange={this.onChangeUpdate}
                                      />
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row row-40">
                                <div class="col-lg-6">
                                  <div class="form-group has-feedback">
                                    <label class="control-label">ตำแหน่ง</label>
                                    <div class="block-control">
                                      <select
                                        className="form-control"
                                        name="position_id"
                                        value={this.state.user.position_id}
                                        onChange={this.onChangeUpdate}
                                      >
                                        {position.map(
                                          ({ id, subject }, index) => (
                                            <option value={id}>
                                              {subject}
                                            </option>
                                          )
                                        )}
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-6">
                                  <div class="form-group has-feedback">
                                    <label class="control-label">สังกัด</label>
                                    <div class="block-control">
                                      <select
                                        className="form-control"
                                        name="affiliate_id"
                                        value={this.state.user.affiliation_id}
                                        onChange={this.onChangeUpdate}
                                      >
                                        {affiliate.map(
                                          ({ id, subject }, index) => (
                                            <option value={id}>
                                              {subject}
                                            </option>
                                          )
                                        )}
                                      </select>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row row-40">
                                <div class="col-lg-6">
                                  <div class="form-group has-feedback">
                                    <label class="control-label">ธนาคาร</label>
                                    <div class="block-control">
                                      <select
                                        className="form-control"
                                        name="bank_id"
                                        value={this.state.user.bank_id}
                                        onChange={this.onChangeUpdate}
                                      >
                                        {bank.map(({ id, subject }, index) => (
                                          <option value={id}>{subject}</option>
                                        ))}
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-6">
                                  <div class="form-group has-feedback">
                                    <label class="control-label">สาขา</label>
                                    <div class="block-control">
                                      <input
                                        type="text"
                                        className="form-control"
                                        name="major_bank"
                                        value={this.state.user.major_bank}
                                        onChange={this.onChangeUpdate}
                                      />
                                      <span
                                        class="form-control-feedback"
                                        aria-hidden="true"
                                      ></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row row-40">
                                <div class="col-lg-6">
                                  <div class="form-group has-feedback">
                                    <label class="control-label">
                                      ชื่อบัญชีธนาคาร
                                    </label>
                                    <div class="block-control">
                                      <input
                                        type="text"
                                        className="form-control"
                                        name="name_bank"
                                        value={this.state.user.name_bank}
                                        onChange={this.onChangeUpdate}
                                      />
                                      <span
                                        class="form-control-feedback"
                                        aria-hidden="true"
                                      ></span>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-6">
                                  <div class="form-group has-feedback">
                                    <label class="control-label">
                                      เลขที่บัญชี
                                    </label>
                                    <div class="block-control">
                                      <input
                                        type="text"
                                        className="form-control"
                                        name="number_bank"
                                        value={this.state.user.number_bank}
                                        onChange={this.onChangeUpdate}
                                      />
                                      <span
                                        class="form-control-feedback"
                                        aria-hidden="true"
                                      ></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              {/* <div class="row row-40">
                                            <div class="col-lg-12">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">อัตราเงินเดือน</label>
                                                    <div class="block-control">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <div style={{position: 'relative'}}>
                                                                <input
                                                                      type="number"
                                                                      className="form-control"
                                                                      name="salary"
                                                                      value={this.state.user.salary}
                                                                      onChange={this.onChangeUpdate}
                                                                    />
                                                                    <span class="form-control-feedback" aria-hidden="true"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-auto">
                                                                <label class="control-label"style={{margin:'0'}}>บาท</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> */}
                              <div class="row row-40">
                                <div class="col-lg-6">
                                  <div class="form-group has-feedback">
                                    <label
                                      class="control-label"
                                      style={{ display: "block" }}
                                    >
                                      เบอร์โทรศัพท์
                                    </label>
                                    <div class="block-control">
                                      <input
                                        type="text"
                                        className="form-control"
                                        name="tel"
                                        value={this.state.user.tel}
                                        onChange={this.onChangeUpdate}
                                      />
                                      <span
                                        class="form-control-feedback"
                                        aria-hidden="true"
                                      ></span>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-6">
                                  <div class="form-group has-feedback">
                                    <label class="control-label">อีเมล</label>
                                    <div class="block-control">
                                      <input
                                        type="email"
                                        className="form-control"
                                        name="email"
                                        value={this.state.user.email}
                                        onChange={this.onChangeUpdate}
                                      />
                                      <span
                                        class="form-control-feedback"
                                        aria-hidden="true"
                                      ></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row row-40">
                                <div class="col-lg-12">
                                  <div class="form-group has-feedback">
                                    <label class="control-label">
                                      หมายเหตุ
                                    </label>
                                    <div class="block-control">
                                      <textarea
                                        type="text"
                                        className="form-control"
                                        name="note"
                                        value={this.state.user.note}
                                        onChange={this.onChangeUpdate}
                                      ></textarea>
                                      <span
                                        class="form-control-feedback"
                                        aria-hidden="true"
                                      ></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  class="login-action"
                  data-aos="fade-down"
                  data-aos-delay="300"
                >
                  <div class="row row-0 align-items-center">
                    <div class="col">
                      <a href="/" class="btn btn-home">
                        <div class="icon">
                          <img
                            src="img/icon/i-btn-home.svg"
                            alt="กลับสู่หน้าหลัก"
                            class="lazy"
                          />
                        </div>
                        กลับสู่หน้าหลัก
                      </a>
                    </div>
                    <div class="col-auto">
                      <a href="javascript:history.back();" class="btn btn-back">
                        <div class="icon">
                          <img
                            src="img/icon/i-btn-back.svg"
                            alt="ย้อนกลับ"
                            class="lazy"
                          />
                        </div>
                        ย้อนกลับ
                      </a>
                      <button
                        type="submit"
                        class="btn btn-submit"
                      >
                        <div class="icon">
                          <img
                            src="img/icon/i-btn-editProfile.svg"
                            alt="แก้ไขข้อมูลส่วนตัว"
                            class="lazy"
                          />
                        </div>
                        บันทึก
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>

        <Footer />
      </div>
    );
  }
}

export default EditProfile;
