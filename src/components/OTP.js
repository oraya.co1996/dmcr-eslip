import React, { Component } from "react";
import Footerin from "./Footerin";
import Headermember from "./Headermember";
import useCountDown from "react-countdown-hook";
const axios = require("axios");

const CountDown = () => {
  const [timeLeft, actions] = useCountDown(60000, 1000);
  return (
    <div>
      {timeLeft > 0 ? (
        <h3 className="mt-3 text-center">
          คุณสามารถขอ OTP ได้อีกครั้งภายใน {timeLeft / 1000} วินาที
        </h3>
      ) : null}
      {timeLeft === 0 ? (
        <div class="action" onClick={() => sendOTP()}>
          <a href="#" class="btn">
            <span class="feather icon-refresh-cw"></span>
            ส่งขอรหัสยืนยันใหม่อีกครั้ง
          </a>
        </div>
        // <button className="btn btn-primary mt-3" onClick={() => sendOTP()}>
        //   Send OTP again
        // </button>
      ) : null}
      <button
        type="button"
        id="start"
        onClick={() => actions.start()}
        style={{ display: "none" }}
      >
        Start
      </button>
    </div>
  );
};

function startTime() {
  console.log("in");
  setTimeout(function(){
    document.getElementById("start").click();
  }, 500);
}

function sendOTP() {
  let body = {
    method: "request_otp",
    identificationNo: localStorage.getItem("id"),
  };
  console.log(body, window.$header);
  axios
    .post(window.$baseUrl + "eslip/api", body, window.$header)
    .then(function (res) {
      console.log(res);
      if (res.data.code == 1001) {
        // alert("ระบบส่ง OTP ให้ท่านใหม่แล้ว กรุณาตรวจสอบอีเมลของคุณ");
        window.location.href = "/otp";
      } else {
        alert(res.data.code + ": " + res.data.msg);
      }
    })
    .catch(function (error) {
      console.log(error);
    });
}

class OTP extends Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };
    this.handleSubmit = this.handleSubmit.bind(this);
    startTime();
  }

  onChangeUpdate = (e) => {
    const { name, value } = e.target;
    this.setState({ value: value });
    this.state.update_text = value;
    switch (name) {
      case "otp":
        this.state.value = this.state.update_text;
        break;
      default:
    }
  };

  handleSubmit(event) {
    event.preventDefault();
    console.log("handleSubmit", this.state.value);
    if (localStorage.getItem("time")) {
      alert(
        "คุณไม่สามารถทำรายการได้ เนื่องจากใส่เลขบัตรประจำตัวประชาชนผิดเกิน 3 ครั้ง กรุณารออีก 24 ชั่วโมง หลังจากการทำรายการครั้งล่าสุด"
      );
    } else {
      console.log(this.state.value);
      let body = {
        method: "login",
        identificationNo: localStorage.getItem("id"),
        otp: this.state.value,
        ipAddress: "192.168.100.77",
      };
      console.log(window.$header);
      axios
        .post(window.$baseUrl + "eslip/api", body, window.$header)
        .then(function (res) {
          console.log(res);
          if (res.data.code == 1001) {
            localStorage.setItem("memberID", res.data.mem_id);
            window.location.href = "/profile";
          } else {
            alert(res.data.code + ": " + res.data.msg);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }

  render() {
    return (
      <div>
        <Headermember />
        <section class="site-container -site-member-page -site-member-content">
          <div class="bg-slide">
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide01.jpg" alt="" class="lazy" />
              </figure>
            </div>
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide02.jpg" alt="" class="lazy" />
              </figure>
            </div>
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide03.jpg" alt="" class="lazy" />
              </figure>
            </div>
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide04.jpg" alt="" class="lazy" />
              </figure>
            </div>
          </div>

          <div class="-site-member-inner">
            <div class="container">
              <form
                onSubmit={this.handleSubmit}
                data-toggle="validator"
                role="form"
                class="form-default"
                method="post"
                novalidate="true"
              >
                <div class="login-box" data-aos="fade-up">
                  <div class="row row-0 align-items-center">
                    <div class="col-auto otp-box">
                      <div
                        class="otp-card"
                        data-aos="fade-right"
                        data-aos-delay="2000"
                      >
                        <div class="icon">
                          <span class="feather icon-refresh-cw"></span>
                        </div>
                        <div class="txt">ส่งขอรหัสยืนยันใหม่อีกครั้ง</div>
                        <CountDown />
                      </div>
                    </div>
                    <div class="col">
                      <div
                        class="content"
                        data-aos="fade-left"
                        data-aos-delay="2000"
                      >
                        <div class="icon">
                          <img
                            src="img/static/brand-log.png"
                            alt="กรมทรัพยากรทางทะเลและชายฝั่ง Department of Marine and Coastal Resources, Thailand"
                            class="lazy"
                          />
                        </div>
                        <div class="desc">
                          <span>ยินดีต้อนรับเข้าสู่ระบบ</span>
                          <br />
                          <strong>ระบบใบแจ้งเงินเดือน ค่าจ้างประจำ</strong>
                          <br />
                          <strong>และค่าตอบแทนพนักงานราชการ (E-Slip)</strong>
                        </div>
                        <div class="desc-sub">
                          กรุณากรอกเลข OTP ที่ส่งไปยังอีเมล test@gmail.com
                          <br />
                          (Ref : DBAXXX)
                        </div>
                        <div class="form-group has-feedback">
                          <div class="block-control">
                            <input
                              class="form-control -otp"
                              type="text"
                              name="otp"
                              value={this.state.value}
                              onChange={this.onChangeUpdate}
                            ></input>
                            <span
                              class="form-control-feedback"
                              aria-hidden="true"
                            ></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  class="login-action"
                  data-aos="fade-down"
                  data-aos-delay="300"
                >
                  <div class="row row-0 align-items-center">
                    <div class="col">
                      <a href="/" class="btn btn-home">
                        <div class="icon">
                          <img
                            src="img/icon/i-btn-home.svg"
                            alt="กลับสู่หน้าหลัก"
                            class="lazy"
                          />
                        </div>
                        กลับสู่หน้าหลัก
                      </a>
                    </div>
                    <div class="col-auto">
                      <a href="javascript:history.back();" class="btn btn-back">
                        <div class="icon">
                          <img
                            src="img/icon/i-btn-back.svg"
                            alt="ย้อนกลับ"
                            class="lazy"
                          />
                        </div>
                        ย้อนกลับ
                      </a>
                      <button class="btn btn-submit" type="submit">
                        <div class="icon">
                          <img
                            src="img/icon/i-btn-submit.svg"
                            alt="ยืนยัน"
                            class="lazy"
                          />
                        </div>
                        ยืนยัน
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>
        <Footerin />
      </div>
    );
  }
}

export default OTP;
