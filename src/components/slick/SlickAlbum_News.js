import React from 'react'
// import { Link, Route } from 'react-router-dom';
import Slider from "react-slick";

export default function SlickAlbum_News() {

    const settings = {
        dots: true,
        infinite: false,
        arrows: false,
        speed: 1500,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    };

    return (
        <React.Fragment>
            <Slider {...settings} className="gallery" data-aos="fade-up">
                <div className="item">
                    <a href="img/upload/about-pjsource.png" className="link" data-fancybox="image">
                        <img src="img/upload/about-pjsource.png" alt="icon" />
                    </a>
                </div>
                <div className="item">
                    <a href="img/upload/about-pjsource.png" className="link" data-fancybox="image">
                        <img src="img/upload/about-pjsource.png" alt="icon" />
                    </a>
                </div>
                <div className="item">
                    <a href="img/upload/about-pjsource.png" className="link" data-fancybox="image">
                        <img src="img/upload/about-pjsource.png" alt="icon" />
                    </a>
                </div>
                <div className="item">
                    <a href="img/upload/about-pjsource.png" className="link" data-fancybox="image">
                        <img src="img/upload/about-pjsource.png" alt="icon" />
                    </a>
                </div>
                <div className="item">
                    <a href="img/upload/about-pjsource.png" className="link" data-fancybox="image">
                        <img src="img/upload/about-pjsource.png" alt="icon" />
                    </a>
                </div>
                <div className="item">
                    <a href="img/upload/about-pjsource.png" className="link" data-fancybox="image">
                        <img src="img/upload/about-pjsource.png" alt="icon" />
                    </a>
                </div>
                <div className="item">
                    <a href="img/upload/about-pjsource.png" className="link" data-fancybox="image">
                        <img src="img/upload/about-pjsource.png" alt="icon" />
                    </a>
                </div>
                <div className="item">
                    <a href="img/upload/about-pjsource.png" className="link" data-fancybox="image">
                        <img src="img/upload/about-pjsource.png" alt="icon" />
                    </a>
                </div>
            </Slider>
        </React.Fragment>
    )
}
