import React from 'react'
// import { Link, Route } from 'react-router-dom';
import Slider from "react-slick";

export default function SlickDownload_About() {

    const settings = {
        dots: true,
        infinite: false,
        arrows: false,
        speed: 1500,
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    speed: 300
                }
            },
        ]
    };

    return (
        <React.Fragment>
            <div className="download-list" data-aos="fade-up">
                <Slider {...settings} className="slider">
                    <div className="item">
                        <div className="wrapper">
                            <a className="link" href="#" title="download">
                                <div className="row row-0 align-items-center">
                                    <div className="col-sm-auto">
                                        <div className="icon">
                                            <span className="feather icon-download" />
                                        </div>
                                    </div>
                                    <div className="col-sm pt-2 pt-sm-0">
                                        <div className="title">download</div>
                                        <div className="info">
                                            <div className="row row-10">
                                                <div className="col-auto">ประเภทไฟล์ : <span>pdf</span></div>
                                                <div className="col-auto">ขนาด : <span>8.52 kb</span></div>
                                                <div className="col-auto">จำนวนดาวน์โหลด : <span>2 ครั้ง</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div className="item">
                        <div className="wrapper">
                            <a className="link" href="#" title="download">
                                <div className="row row-0 align-items-center">
                                    <div className="col-sm-auto">
                                        <div className="icon">
                                            <span className="feather icon-download" />
                                        </div>
                                    </div>
                                    <div className="col-sm pt-2 pt-sm-0">
                                        <div className="title">download</div>
                                        <div className="info">
                                            <div className="row row-10">
                                                <div className="col-auto">ประเภทไฟล์ : <span>pdf</span></div>
                                                <div className="col-auto">ขนาด : <span>8.52 kb</span></div>
                                                <div className="col-auto">จำนวนดาวน์โหลด : <span>2 ครั้ง</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div className="item">
                        <div className="wrapper">
                            <a className="link" href="#" title="download">
                                <div className="row row-0 align-items-center">
                                    <div className="col-sm-auto">
                                        <div className="icon">
                                            <span className="feather icon-download" />
                                        </div>
                                    </div>
                                    <div className="col-sm pt-2 pt-sm-0">
                                        <div className="title">download</div>
                                        <div className="info">
                                            <div className="row row-10">
                                                <div className="col-auto">ประเภทไฟล์ : <span>pdf</span></div>
                                                <div className="col-auto">ขนาด : <span>8.52 kb</span></div>
                                                <div className="col-auto">จำนวนดาวน์โหลด : <span>2 ครั้ง</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </Slider>
            </div>
        </React.Fragment>
    )
}
