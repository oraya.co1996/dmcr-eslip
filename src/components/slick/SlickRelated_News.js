import React from 'react'
import { Link, Route, useHistory } from 'react-router-dom';
import Slider from "react-slick";

export default function SlickRelated_News() {

    const settings = {
        dots: true,
        infinite: false,
        arrows: false,
        speed: 1500,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    return (
        <React.Fragment>
            <Slider {...settings} className="other-news" data-aos="fade-up">
                <div className="item">
                    <a className="link" href="detail.php">
                        <div className="hexGrid">
                            <div className="hex">
                                <div className="hexIn">
                                    <div className="hexLink">
                                        <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-1.png)' }} />
                                        <div className="date">31 มีนาคม 2564</div>
                                        <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                        <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div className="item">
                    <a className="link" href="detail.php">
                        <div className="hexGrid">
                            <div className="hex">
                                <div className="hexIn">
                                    <div className="hexLink">
                                        <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-1.png)' }} />
                                        <div className="date">31 มีนาคม 2564</div>
                                        <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                        <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div className="item">
                    <a className="link" href="detail.php">
                        <div className="hexGrid">
                            <div className="hex">
                                <div className="hexIn">
                                    <div className="hexLink">
                                        <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-1.png)' }} />
                                        <div className="date">31 มีนาคม 2564</div>
                                        <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                        <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div className="item">
                    <a className="link" href="detail.php">
                        <div className="hexGrid">
                            <div className="hex">
                                <div className="hexIn">
                                    <div className="hexLink">
                                        <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-1.png)' }} />
                                        <div className="date">31 มีนาคม 2564</div>
                                        <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                        <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div className="item">
                    <a className="link" href="detail.php">
                        <div className="hexGrid">
                            <div className="hex">
                                <div className="hexIn">
                                    <div className="hexLink">
                                        <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-1.png)' }} />
                                        <div className="date">31 มีนาคม 2564</div>
                                        <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                        <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div className="item">
                    <a className="link" href="detail.php">
                        <div className="hexGrid">
                            <div className="hex">
                                <div className="hexIn">
                                    <div className="hexLink">
                                        <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-1.png)' }} />
                                        <div className="date">31 มีนาคม 2564</div>
                                        <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                        <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div className="item">
                    <a className="link" href="detail.php">
                        <div className="hexGrid">
                            <div className="hex">
                                <div className="hexIn">
                                    <div className="hexLink">
                                        <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-1.png)' }} />
                                        <div className="date">31 มีนาคม 2564</div>
                                        <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                        <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div className="item">
                    <a className="link" href="detail.php">
                        <div className="hexGrid">
                            <div className="hex">
                                <div className="hexIn">
                                    <div className="hexLink">
                                        <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-1.png)' }} />
                                        <div className="date">31 มีนาคม 2564</div>
                                        <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                        <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div className="item">
                    <a className="link" href="detail.php">
                        <div className="hexGrid">
                            <div className="hex">
                                <div className="hexIn">
                                    <div className="hexLink">
                                        <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-1.png)' }} />
                                        <div className="date">31 มีนาคม 2564</div>
                                        <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                        <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div className="item">
                    <a className="link" href="detail.php">
                        <div className="hexGrid">
                            <div className="hex">
                                <div className="hexIn">
                                    <div className="hexLink">
                                        <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-1.png)' }} />
                                        <div className="date">31 มีนาคม 2564</div>
                                        <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                        <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </Slider>
        </React.Fragment>
    )
}
