import React from 'react'
import { Link, Route } from 'react-router-dom';
import Slider from "react-slick";

export default function TopGraphic() {

    const settings = {
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1000,
        rows: 1,
        slidesPerRow:1,
        fade: true,
        cssEase: 'linear',
        responsive: [
            {
                breakpoint: 575,
                settings: {
                    rows: 1,
                    slidesPerRow:1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    rows: 1,
                    slidesPerRow:1
                }
            },
            {
                breakpoint: 1366,
                settings: {
                    rows: 1,
                    slidesPerRow:1
                }
            }
        ]
    };

    return (
        <React.Fragment>
            <div className="topgraphic" data-aos="fade-zoom-in">
                    <Slider {...settings} className="slider">
                        <div className="item">
                            <Link to="" className="lonk">
                                <div className="b-topgraphic">
                                    <figure className="cover">
                                        <img src="img/background/tp-img1.png" alt="tp-img-1" />
                                    </figure>
                                    <div className="container">
                                        <div className="b-text">
                                            <div className="top-title" data-aos="fade-right" data-aos-delay={300}>DMCR</div>
                                            <div className="h-title" data-aos="fade-left" data-aos-delay={600}>E-SLIP</div>
                                            <div className="bottom-title" data-aos="fade-right" data-aos-delay={900}>กรมทรัพยากรทางทะเลและชายฝั่ง</div>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className="item">
                            <Link to="" className="lonk">
                                <div className="b-topgraphic">
                                    <figure className="cover">
                                        <img src="img/background/tp-img2.png" alt="tp-img-2" />
                                    </figure>
                                    <div className="container">
                                        <div className="b-text">
                                            <div className="top-title" data-aos="fade-right" data-aos-delay={300}>DMCR</div>
                                            <div className="h-title" data-aos="fade-left" data-aos-delay={600}>E-SLIP</div>
                                            <div className="bottom-title" data-aos="fade-right" data-aos-delay={900}>กรมทรัพยากรทางทะเลและชายฝั่ง</div>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </Slider>
                </div>
        </React.Fragment>
    )
}
