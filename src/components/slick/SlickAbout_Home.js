import React from 'react'
import { Link, Route } from 'react-router-dom';
import Slider from "react-slick";

function SampleNextArrow() {
    return (
        
        <div className="slick-next"><img src="img/icon/arrows-right.svg" /></div>
    );
}

function SamplePrevArrow() {
    return (
        <div className="slick-prev"><img src="img/icon/arrows-left.svg" /></div>
    );
}

export default function TopGraphic() {

    const settings = {
        prevArrow:<SampleNextArrow/>,
        nextArrow:<SamplePrevArrow/>,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        autoplay: false,
        autoplaySpeed: 3000,
        speed: 1000,
        responsive: [
            // {
            //     breakpoint: 575,
            //     settings: {
            //         rows: 1,
            //         slidesPerRow:1
            //     }
            // },
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    speed: 600,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    arrows: false,
                    speed: 600
                }
            }
        ]
    };

    return (
        <React.Fragment>
            <div className="wg-about">
                <div className="container">
                    <div className="h-title text-primary text-center" data-aos="fade-up">เกี่ยวกับโครงการ</div>
                    <div className="wg-about-list" data-aos="fade-up">
                        <Slider {...settings} className="slider">
                            <div className="item">
                                <div className="wrapper">
                                    <Link to="/about" className="link">
                                        <div className="cover">
                                            <div className="b-img">
                                                <img src="img/static/ab-1.png" alt="ab-1" />
                                            </div>
                                        </div>
                                        <div className="content">
                                            <div className="title text-primary">กฎระเบียบ</div>
                                            <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                            <div className="item">
                                <div className="wrapper">
                                    <Link to="/about" className="link">
                                        <div className="cover">
                                            <div className="b-img">
                                                <img src="img/static/ab-2.png" alt="ab-2" />
                                            </div>
                                        </div>
                                        <div className="content">
                                            <div className="title text-primary">ที่มาโครงการ</div>
                                            <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                            <div className="item">
                                <div className="wrapper">
                                    <Link to="/about" className="link">
                                        <div className="cover">
                                            <div className="b-img">
                                                <img src="img/static/ab-3.png" alt="ab-3" />
                                            </div>
                                        </div>
                                        <div className="content">
                                            <div className="title text-primary">นโยบาย</div>
                                            <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                            <div className="item">
                                <div className="wrapper">
                                    <Link to="/about" className="link">
                                        <div className="cover">
                                            <div className="b-img">
                                                <img src="img/static/ab-1.png" alt="ab-1" />
                                            </div>
                                        </div>
                                        <div className="content">
                                            <div className="title text-primary">กฎระเบียบ</div>
                                            <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                            <div className="item">
                                <div className="wrapper">
                                    <Link to="/about" className="link">
                                        <div className="cover">
                                            <div className="b-img">
                                                <img src="img/static/ab-2.png" alt="ab-2" />
                                            </div>
                                        </div>
                                        <div className="content">
                                            <div className="title text-primary">ที่มาโครงการ</div>
                                            <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                            <div className="item">
                                <div className="wrapper">
                                    <Link to="/about" className="link">
                                        <div className="cover">
                                            <div className="b-img">
                                                <img src="img/static/ab-3.png" alt="ab-3" />
                                            </div>
                                        </div>
                                        <div className="content">
                                            <div className="title text-primary">นโยบาย</div>
                                            <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                            <div className="item">
                                <div className="wrapper">
                                    <Link to="/about" className="link">
                                        <div className="cover">
                                            <div className="b-img">
                                                <img src="img/static/ab-1.png" alt="ab-1" />
                                            </div>
                                        </div>
                                        <div className="content">
                                            <div className="title text-primary">กฎระเบียบ</div>
                                            <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                            <div className="item">
                                <div className="wrapper">
                                    <Link to="/about" className="link">
                                        <div className="cover">
                                            <div className="b-img">
                                                <img src="img/static/ab-2.png" alt="ab-2" />
                                            </div>
                                        </div>
                                        <div className="content">
                                            <div className="title text-primary">ที่มาโครงการ</div>
                                            <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                            <div className="item">
                                <div className="wrapper">
                                    <Link to="/about" className="link">
                                        <div className="cover">
                                            <div className="b-img">
                                                <img src="img/static/ab-3.png" alt="ab-3" />
                                            </div>
                                        </div>
                                        <div className="content">
                                            <div className="title text-primary">นโยบาย</div>
                                            <div className="desc">แสดงข้อมูลกรณีผู้ประกอบการที่สนใจ ต้องการทราบรายละเอียดเพิ่มเติม และหน่วยงาน</div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </Slider>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}
