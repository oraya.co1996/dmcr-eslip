import React, { Component } from "react";
import Footerin from "./Footerin";
import Headermember from "./Headermember";
const axios = require("axios");
let time = 0;

class CheckIdNo extends Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    if (localStorage.getItem("time")) {
      alert(
        "คุณไม่สามารถทำรายการได้ เนื่องจากใส่เลขบัตรประจำตัวประชาชนผิดเกิน 3 ครั้ง กรุณารออีก 24 ชั่วโมง หลังจากการทำรายการครั้งล่าสุด"
      );
    } else {
      console.log(this.state.value);
      event.preventDefault();
      let body = {
        method: "request_otp",
        identificationNo: this.state.value,
      };
      console.log(window.$header);
      axios
        .post(window.$baseUrl + "eslip/api", body, window.$header)
        .then(function (res) {
          console.log(res);
          if (res.data.code === 1001) {
            localStorage.setItem("id", body.identificationNo);
            window.location.href = "/otp";
          } else if (res.data.code === 1000) {
            time += 1;
            if (time >= 3) {
              localStorage.setItem("time", new Date().getTime());
              window.location.href = "/";
            }
            alert(res.data.code + ": " + res.data.msg);
          } else if (res.data.code === 1007) {
            localStorage.removeItem("token");
            alert("กรุณาลองอีกครั้ง");
          } else {
            alert(res.data.code + ": " + res.data.msg);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }
  render() {
    return (
      <div>
        <Headermember />
        <section class="site-container -site-member-page -site-member-content">
          <div class="bg-slide">
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide01.jpg" alt="" class="lazy" />
              </figure>
            </div>
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide02.jpg" alt="" class="lazy" />
              </figure>
            </div>
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide03.jpg" alt="" class="lazy" />
              </figure>
            </div>
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide04.jpg" alt="" class="lazy" />
              </figure>
            </div>
          </div>

          <div class="-site-member-inner">
            <div class="container">
              <form
                data-toggle="validator"
                role="form"
                class="form-default"
                method="post"
                novalidate="true"
                onSubmit={this.handleSubmit}
              >
                <div class="login-box" data-aos="fade-up">
                  <div class="row row-0 align-items-center">
                    <div class="col-auto citizen-box">
                      <div
                        class="citizen-card"
                        data-aos="fade-right"
                        data-aos-delay="2000"
                      >
                        <img
                          src="img/static/citizen-card.png"
                          alt="citizen-card"
                          class="lazy"
                        />
                      </div>
                    </div>
                    <div class="col">
                      <div
                        class="content"
                        data-aos="fade-left"
                        data-aos-delay="2000"
                      >
                        <div class="icon">
                          <img
                            src="img/static/brand-log.png"
                            alt="กรมทรัพยากรทางทะเลและชายฝั่ง Department of Marine and Coastal Resources, Thailand"
                            class="lazy"
                          />
                        </div>
                        <div class="desc">
                          <span>ยินดีต้อนรับเข้าสู่ระบบ</span>
                          <br />
                          <strong>ระบบใบแจ้งเงินเดือน ค่าจ้างประจำ</strong>
                          <br />
                          <strong>และค่าตอบแทนพนักงานราชการ (E-Slip)</strong>
                        </div>
                        <div class="desc">
                          <strong>กรุณาใส่เลขประจำตัวประชาชน</strong>
                        </div>
                        <div class="form-group has-feedback">
                          <div class="block-control">
                            <div
                              class="input-group show-password"
                            >
                              <input
                                type="text"
                                className="form-control"
                                value={this.state.value}
                                onChange={this.handleChange}
                              />

                              {/* <span
                                class="form-control-feedback"
                                aria-hidden="true"
                              ></span>
                              <div class="input-group-addon">
                                <a href="javascript:void(0)" class="link">
                                  <span
                                    class="icon on"
                                    aria-hidden="true"
                                  ></span>
                                </a>
                              </div> */}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  class="login-action"
                  data-aos="fade-down"
                  data-aos-delay="300"
                >
                  <div class="row row-0 align-items-center">
                    <div class="col">
                      <a href="/" class="btn btn-home">
                        <div class="icon">
                          <img
                            src="img/icon/i-btn-home.svg"
                            alt="กลับสู่หน้าหลัก"
                            class="lazy"
                          />
                        </div>
                        กลับสู่หน้าหลัก
                      </a>
                    </div>
                    <div class="col-auto">
                      <a href="javascript:history.back();" class="btn btn-back">
                        <div class="icon">
                          <img
                            src="img/icon/i-btn-back.svg"
                            alt="ย้อนกลับ"
                            class="lazy"
                          />
                        </div>
                        ย้อนกลับ
                      </a>

                      <button
                        class="btn btn-submit"
                        type="submit"
                      >
                        <div class="icon">
                          <img
                            src="img/icon/i-btn-submit.svg"
                            alt="ยืนยัน"
                            class="lazy"
                          />
                        </div>
                        ยืนยัน
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>

        <Footerin />
      </div>
    );
  }
}

export default CheckIdNo;
