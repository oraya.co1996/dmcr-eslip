import React from 'react';
import { Link } from 'react-router-dom';

function Header() {
    return (
        <React.Fragment>
            <header className="site-header">
                <div className="main-header">
                    {/* <div class="container"> */}
                    <div className="row no-gutters">
                        <div className="col-8 col-md">
                            <div className="brand" data-aos="fade-right">
                            <Link to="/" title="กรมทรัพยากรทางทะเลและชายฝั่ง Department of Marine and Coastal Resources, Thailand">
                                    <img src="img/static/brand.png" title="กรมทรัพยากรทางทะเลและชายฝั่ง Department of Marine and Coastal Resources, Thailand" alt="กรมทรัพยากรทางทะเลและชายฝั่ง Department of Marine and Coastal Resources, Thailand" />
                                </Link>
                                <div className="b-brand" data-aos="fade-right" data-aos-delay={300}>
                                <Link to="/" className="brand-txt link" title="กรมทรัพยากรทางทะเลและชายฝั่ง Department of Marine and Coastal Resources, Thailand">
                                        <span>DMCR SLIP</span><br />
                                        กรมทรัพยากรทางทะเลและชายฝั่ง
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className="col-4 col-md-auto">
                            <div className="b-btn-header">
                                <div className="action" data-aos="fade-left" data-aos-delay={300}>
                                    <a href="/check-idno" className="btn btn-light btn-medium btn-round-5 btn-login">เข้าสู่ระบบ</a>
                                </div>
                                <div className="action" data-aos="fade-left">
                                    <a href="https://www.dmcr.go.th" target="_blank" className="btn btn-light btn-medium btn-round-5">เข้าสู่เว็บ ทช.</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* </div> */}
                </div>
                <div className="overlay" data-toggle="menu-overlay" />
            </header>
        </React.Fragment>
    );
}

export default Header;