import React, { Component } from 'react';
import Footer from './Footer';
import Header from './Header';

class Login extends Component {
  render() {
    return (
        <div>
            <Header />
            <section className="site-container mt-5 bodyh">
                <div className="container mt-5">
                    <div className="row my-5">
                        <h2 className="mt-5">Login</h2>
                        <input type="text" className="form-control" />
                    </div>                    
                </div>
            </section>
            <Footer />
        </div>
    );
  }
}

export default Login;