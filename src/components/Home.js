import React, { Component } from 'react';
import Headermember from "./Headermember";
import Footerin from './Footerin';
import Logout from './Logout';
const axios = require("axios");

const header = { Authorization: "Bearer " + localStorage.getItem("token") };

function edit() {
  window.location.href = "/edit-profile";
}

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        affiliation: "",
        bank: "",
        email: "",
        identificationNo: "",
        major_bank: "",
        mem_id: 0,
        name: "",
        name_bank: "",
        note: "",
        number_bank: "",
        pic: "",
        position: "",
        salary: 0,
        tel: "",
      },
    };
    this.getProfile();
  }

  onChangeUpdate = (e) => {
    const { name, value } = e.target;
    this.setState({ value: value });
    this.state.update_text = value;
    switch (name) {
      case "affiliation":
        this.state.user.affiliation = this.state.update_text;
        break;
      case "bank":
        this.state.user.bank = this.state.update_text;
        break;
      case "email":
        this.state.user.email = this.state.update_text;
        break;
      case "identificationNo":
        this.state.user.identificationNo = this.state.update_text;
        break;
      case "major_bank":
        this.state.user.major_bank = this.state.update_text;
        break;
      case "mem_id":
        this.state.user.mem_id = this.state.update_text;
        break;
      case "name":
        this.state.user.name = this.state.update_text;
        break;
      case "name_bank":
        this.state.user.name_bank = this.state.update_text;
        break;
      case "note":
        this.state.user.note = this.state.update_text;
        break;
      case "number_bank":
        this.state.user.number_bank = this.state.update_text;
        break;
      case "pic":
        this.state.user.pic = this.state.update_text;
        break;
      case "position":
        this.state.user.position = this.state.update_text;
        break;
      case "salary":
        this.state.user.salary = this.state.update_text;
        break;
      case "tel":
        this.state.user.tel = this.state.update_text;
        break;
      default:
    }
  };

  getProfile() {
    let currentComponent = this;
    let config = {
      method: "get",
      url:
        window.$baseUrl +
        "eslip/user?method=getProfile&mem_id=" +
        localStorage.getItem("memberID") +
        "&masterkey=official",
      headers: header,
    };
    axios(config)
      .then(function (res) {
        console.log(res);
        if (res.data.code == 1001) {
          currentComponent.setState({ user: res.data.user_info });
        } else {
          alert(res.data.code + ": " + res.data.msg);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return (
        <div>
            <Headermember />
            {/* <section className="site-container mt-5 bodyh">
                <div className="container mt-5">
                    <div className="row my-5">
                        <h2 className="mt-5">Home</h2>
                    </div>                    
                </div>
            </section> */}
                    <section class="site-container -site-member-page -site-member-content">

                    <div class="bg-slide">
	<div class="item">
		<figure class="cover">
			<img src="img/upload/bg-slide01.jpg" alt="" class="lazy" />
		</figure>
	</div>
	<div class="item">
		<figure class="cover">
			<img src="img/upload/bg-slide02.jpg" alt="" class="lazy" />
		</figure>
	</div>
	<div class="item">
		<figure class="cover">
			<img src="img/upload/bg-slide03.jpg" alt="" class="lazy" />
		</figure>
	</div>
	<div class="item">
		<figure class="cover">
			<img src="img/upload/bg-slide04.jpg" alt="" class="lazy" />
		</figure>
	</div>
</div>


<div class="-site-member-inner">
    <div class="container">
        
        <div class="member-welcome">
            <nav class="nav-list">
                <ul class="item-list">
                    <li  data-aos="fade-up">
                        <a href="index.php" class="link">
                            <div class="icon">
                                <img src="img/icon/i-mem-wel-nav01.svg" alt="หน้าหลัก" class="lazy" />
                                <svg xmlns="http://www.w3.org/2000/svg"  viewbox="0 0 173.20508075688772 200">
                                    <path  d="M77.94228634059948 
                                        4.999999999999999Q86.60254037844386 0 95.26279441628824 
                                        4.999999999999999L164.54482671904333 45Q173.20508075688772 50 
                                        173.20508075688772 60L173.20508075688772 140Q173.20508075688772 150 
                                        164.54482671904333 155L95.26279441628824 195Q86.60254037844386 200 
                                        77.94228634059948 195L8.660254037844387 155Q0 150 0 140L0 60Q0 50 
                                        8.660254037844387 45Z">
                                    </path>
                                </svg>
                            </div>
                            <div class="txt">หน้าหลัก</div>
                        </a>
                    </li>
                    <li data-aos="fade-up" data-aos-delay="300">
                        <a href="news.php" class="link">
                            <div class="icon">
                                <img src="img/icon/i-mem-wel-nav02.svg" alt="ข่าวสาร" class="lazy" />
                                <svg xmlns="http://www.w3.org/2000/svg"  viewbox="0 0 173.20508075688772 200">
                                    <path  d="M77.94228634059948 
                                        4.999999999999999Q86.60254037844386 0 95.26279441628824 
                                        4.999999999999999L164.54482671904333 45Q173.20508075688772 50 
                                        173.20508075688772 60L173.20508075688772 140Q173.20508075688772 150 
                                        164.54482671904333 155L95.26279441628824 195Q86.60254037844386 200 
                                        77.94228634059948 195L8.660254037844387 155Q0 150 0 140L0 60Q0 50 
                                        8.660254037844387 45Z">
                                    </path>
                                </svg>
                            </div>
                            <div class="txt">ข่าวสาร</div>
                        </a>
                    </li>
                    <li data-aos="fade-up" data-aos-delay="600">
                        <a href="about.php" class="link">
                            <div class="icon">
                                <img src="img/icon/i-mem-wel-nav03.svg" alt="เกี่ยวกับโครงการ" class="lazy" />
                                <svg xmlns="http://www.w3.org/2000/svg"  viewbox="0 0 173.20508075688772 200">
                                    <path  d="M77.94228634059948 
                                        4.999999999999999Q86.60254037844386 0 95.26279441628824 
                                        4.999999999999999L164.54482671904333 45Q173.20508075688772 50 
                                        173.20508075688772 60L173.20508075688772 140Q173.20508075688772 150 
                                        164.54482671904333 155L95.26279441628824 195Q86.60254037844386 200 
                                        77.94228634059948 195L8.660254037844387 155Q0 150 0 140L0 60Q0 50 
                                        8.660254037844387 45Z">
                                    </path>
                                </svg>
                            </div>
                            <div class="txt">เกี่ยวกับโครงการ</div>
                        </a>
                    </li>
                </ul>
            </nav>
            <div class="content">
                <div class="thumb" data-aos="fade-in" data-aos-delay="900">
                    <a href="profile.php" class="link">
                        <figure class="cover">
                            <img src="img/upload/user.jpg" alt="ข้าราชการ ลินดา สุทธิกาณจน์" class="lazy" />
                        </figure>
                        <div class="ojb-shadow"></div>
                    </a>
                </div>
                <div class="name" data-aos="fade-up">นางสาวลินดา สุทธิกาณจน์</div>
                <div class="career" data-aos="fade-up">ข้าราชการ</div>
                <div class="job-title" data-aos="fade-up">นักวิชาการเงินและบัญชี</div>
            </div>
        </div>

        <div class="member-nav">
            <ul class="item-list">
                <li class="item"data-aos="fade-up" data-aos-delay="300">
                    <a href="profile.php" class="link">
                        <div class="wrapper item-matchHeight" >
                            <div class="notifications">0</div>
                            <div class="icon">
                                <img src="img/icon/i-mem-nav01.svg" alt="ข้อมูลส่วนตัว" class="lazy" />
                            </div>
                            <div class="content">
                                <div class="title">ข้อมูลส่วนตัว</div>
                                <div class="desc">
                                    ข้อมูลส่วนตัวของพนักงานของกรมทรัพยากร
                                    ทางทะเลและชายฝั่ง
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="item"data-aos="fade-up" data-aos-delay="300">
                    <a href="slip.php" class="link">
                        <div class="wrapper item-matchHeight" >
                            <div class="notifications">0</div>
                            <div class="icon">
                                <img src="img/icon/i-mem-nav02.svg" alt="ข้อมูลใบรับรองเงินเดือน" class="lazy" />
                            </div>
                            <div class="content">
                                <div class="title">ข้อมูลใบรับรองเงินเดือน</div>
                                <div class="desc">
                                    ข้อมูลใบรับรองเงินเดือนของพนักงานของ
                                    กรมทรัพยากรทางทะเลและชายฝั่ง
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="item"data-aos="fade-up" data-aos-delay="300">
                    <a href="certificate.php" class="link">
                        <div class="wrapper item-matchHeight" >
                            <div class="notifications">0</div>
                            <div class="icon">
                                <img src="img/icon/i-mem-nav03.svg" alt="ข้อมูลหนังสือรับรองเงินเดือน" class="lazy" />
                            </div>
                            <div class="content">
                                <div class="title">ข้อมูลหนังสือรับรองเงินเดือน</div>
                                <div class="desc">
                                    ข้อมูลหนังสือรับรองเงินเดือนของพนักงาน
                                    ของกรมทรัพยากรทางทะเลและชายฝั่ง
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="item"data-aos="fade-up" data-aos-delay="300">
                    <a href="approve-slip.php" class="link">
                        <div class="wrapper item-matchHeight" >
                            <div class="notifications active">5</div>
                            <div class="icon">
                                <img src="img/icon/i-mem-nav04.svg" alt="อนุมัติใบรับเงินเดือน" class="lazy" />
                            </div>
                            <div class="content">
                                <div class="title">อนุมัติใบรับเงินเดือน</div>
                                <div class="desc">
                                    อนุมัติใบรับเงินเดือนให้แก่พนักงานของ
                                    กรมทรัพยากรทางทะเลและชายฝั่ง
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="item"data-aos="fade-up" data-aos-delay="300">
                    <a href="approve-certificate.php" class="link">
                        <div class="wrapper item-matchHeight" >
                            <div class="notifications active">5</div>
                            <div class="icon">
                                <img src="img/icon/i-mem-nav05.svg" alt="" class="lazy" />
                            </div>
                            <div class="content">
                                <div class="title">อนุมัติหนังสือรับรองเงินเดือน</div>
                                <div class="desc">
                                    อนุมัติหนังสือรับรองเงินเดือนให้แก่พนักงานของ
                                    กรมทรัพยากรทางทะเลและชายฝั่ง
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>

    </div>
</div>

</section>
            <Footerin />
        </div>
    );
  }
}

export default Home;