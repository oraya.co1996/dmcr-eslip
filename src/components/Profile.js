import React, { Component } from "react";
import Footer from "./Footer";
import Headermember from "./Headermember";
const axios = require("axios");

const header = { Authorization: "Bearer " + localStorage.getItem("token") };

function edit() {
  window.location.href = "/edit-profile";
}

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        affiliation: "",
        bank: "",
        email: "",
        identificationNo: "",
        major_bank: "",
        mem_id: 0,
        name: "",
        name_bank: "",
        note: "",
        number_bank: "",
        pic: "",
        position: "",
        salary: 0,
        tel: "",
      },
    };
    this.getProfile();
  }

  onChangeUpdate = (e) => {
    const { name, value } = e.target;
    this.setState({ value: value });
    this.state.update_text = value;
    switch (name) {
      case "affiliation":
        this.state.user.affiliation = this.state.update_text;
        break;
      case "bank":
        this.state.user.bank = this.state.update_text;
        break;
      case "email":
        this.state.user.email = this.state.update_text;
        break;
      case "identificationNo":
        this.state.user.identificationNo = this.state.update_text;
        break;
      case "major_bank":
        this.state.user.major_bank = this.state.update_text;
        break;
      case "mem_id":
        this.state.user.mem_id = this.state.update_text;
        break;
      case "name":
        this.state.user.name = this.state.update_text;
        break;
      case "name_bank":
        this.state.user.name_bank = this.state.update_text;
        break;
      case "note":
        this.state.user.note = this.state.update_text;
        break;
      case "number_bank":
        this.state.user.number_bank = this.state.update_text;
        break;
      case "pic":
        this.state.user.pic = this.state.update_text;
        break;
      case "position":
        this.state.user.position = this.state.update_text;
        break;
      case "salary":
        this.state.user.salary = this.state.update_text;
        break;
      case "tel":
        this.state.user.tel = this.state.update_text;
        break;
      default:
    }
  };

  getProfile() {
    let currentComponent = this;
    let config = {
      method: "get",
      url:
        window.$baseUrl +
        "eslip/user?method=getProfile&mem_id=" +
        localStorage.getItem("memberID") +
        "&masterkey=official",
      headers: header,
    };
    axios(config)
      .then(function (res) {
        console.log(res);
        if (res.data.code == 1001) {
          currentComponent.setState({ user: res.data.user_info });
        } else {
          alert(res.data.code + ": " + res.data.msg);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        <Headermember />
        <section class="site-container -site-member-page -site-member-content">
          <div class="bg-slide">
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide01.jpg" alt="" class="lazy" />
              </figure>
            </div>
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide02.jpg" alt="" class="lazy" />
              </figure>
            </div>
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide03.jpg" alt="" class="lazy" />
              </figure>
            </div>
            <div class="item">
              <figure class="cover">
                <img src="img/upload/bg-slide04.jpg" alt="" class="lazy" />
              </figure>
            </div>
          </div>

          <div class="-site-member-inner">
            <div class="container">
              <div class="member-default">
                <div class="member-head">
                  <div
                    class="page-tag -profile"
                    data-aos="fade-down"
                    data-aos-delay="300"
                  >
                    <div class="icon">
                      <img
                        src="img/icon/i-mem-nav01.svg"
                        alt="ข้อมูลส่วนตัว"
                        class="lazy"
                      />
                    </div>
                    <div class="txt">ข้อมูลส่วนตัว</div>
                  </div>
                </div>
                <div class="member-body">
                  <div class="member-profile">
                    <div class="row row-0">
                      <div class="col-auto">
                        <div class="profile-image">
                          <div
                            class="thumb"
                            data-aos="fade-in"
                            data-aos-delay="900"
                          >
                            <figure class="cover">
                              {this.state.user.pic ? (
                                <img src={this.state.user.pic} class="lazy" />
                              ) : (
                                <img
                                  src="img/upload/user.jpg"
                                  alt=""
                                  class="lazy"
                                />
                              )}
                            </figure>
                            <div class="ojb-shadow"></div>
                          </div>
                          <div class="content">
                            <div class="name" data-aos="fade-up">
                              {this.state.user.name}
                            </div>
                            <div class="career" data-aos="fade-up">
                              ข้าราชการ
                            </div>
                            <div class="job-title" data-aos="fade-up">
                              นักวิชาการเงินและบัญชี
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col">
                        <div
                          class="profile-content"
                          data-aos="fade-in"
                          data-aos-delay="400"
                        >
                          <div class="inner">
                            <div class="h-title">
                              <div class="icon">
                                <img
                                  src="img/icon/i-user.svg"
                                  alt="ข้อมูลส่วนตัว"
                                  class="lazy"
                                />
                              </div>
                              ข้อมูลส่วนตัว
                            </div>
                            <table class="content">
                              <tbody>
                                <tr>
                                  <td>
                                    <strong>เลขประจำตัวประชาชน:</strong>
                                  </td>
                                  <td>
                                    <span>
                                      {" "}
                                      {this.state.user.identificationNo}
                                    </span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <strong>ตำแหน่ง</strong>
                                  </td>
                                  <td>
                                    <span>{this.state.user.position}</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <strong>สังกัด</strong>
                                  </td>
                                  <td>
                                    <span>{this.state.user.affiliation}</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <strong>ธนาคาร</strong>
                                  </td>
                                  <td>
                                    <span>{this.state.user.bank}</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <strong>สาขา</strong>
                                  </td>
                                  <td>
                                    <span>{this.state.user.major_bank}</span>
                                  </td>
                                </tr>
                                {/* <tr>
                                                <td>
                                                    <strong>ชื่อบัญชีธนาคาร</strong>
                                                </td>
                                                <td>
                                                    <span>{this.state.user.name_bank}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>อัตราเงินเดือน</strong>
                                                </td>
                                                <td>
                                                    <span>{this.state.user.salary}</span>
                                                </td>
                                            </tr> */}
                                <tr>
                                  <td>
                                    <strong>เลขที่บัญชี</strong>
                                  </td>
                                  <td>
                                    <span>{this.state.user.number_bank}</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <strong>เบอร์โทรศัพท์</strong>
                                  </td>
                                  <td>
                                    <span>{this.state.user.tel}</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <strong>อีเมล</strong>
                                  </td>
                                  <td>
                                    <span>{this.state.user.email}</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <strong>หมายเหตุ</strong>
                                  </td>
                                  <td>
                                    <span>{this.state.user.note}</span>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div
                class="login-action"
                data-aos="fade-down"
                data-aos-delay="300"
              >
                <div class="row row-0 align-items-center">
                  <div class="col">
                    <a href="/" class="btn btn-home">
                      <div class="icon">
                        <img
                          src="img/icon/i-btn-home.svg"
                          alt="กลับสู่หน้าหลัก"
                          class="lazy"
                        />
                      </div>
                      กลับสู่หน้าหลัก
                    </a>
                  </div>
                  <div class="col-auto">
                    <a href="javascript:history.back();" class="btn btn-back">
                      <div class="icon">
                        <img
                          src="img/icon/i-btn-back.svg"
                          alt="ย้อนกลับ"
                          class="lazy"
                        />
                      </div>
                      ย้อนกลับ
                    </a>
                    <button
                      class="btn btn-submit"
                      type="button"
                      onClick={() => edit()}
                    >
                      <div class="icon">
                        <img
                          src="img/icon/i-btn-editProfile.svg"
                          alt="แก้ไขข้อมูลส่วนตัว"
                          class="lazy"
                        />
                      </div>
                      แก้ไขข้อมูลส่วนตัว
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

export default Profile;
