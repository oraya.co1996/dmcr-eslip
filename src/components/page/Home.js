import React from 'react';
import { Link, Route } from 'react-router-dom';
import TopGraphic from '../slick/TopGraphic';
import SlickAbout_Home from '../slick/SlickAbout_Home';

function Home() {
    return (
        <React.Fragment>
            <section className="site-container">

                <TopGraphic />

                {/* news */}
                <div className="wg-news" data-aos="fade-up">
                    <div data-aos="zoom-in-left" data-aos-delay={300}>
                        <div className="mn mn-lg"><img src="img/background/w810-lg.png" /></div>
                    </div>
                    <div data-aos="zoom-in-right">
                        <div className="mn mn-sm"><img src="img/background/w810-sm.png" /></div>
                    </div>
                    <div className="container">
                        <div className="h-title text-light">ข่าวสาร &amp; กิจกรรม</div>
                        <div className="b-hex">
                            <ul className="hexGrid">
                                <li className="hex" data-aos="flip-left" data-aos-delay={300}>
                                    <div className="hexIn">
                                        <Link className="hexLink" to="/news-detail">
                                            <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-1.png)' }} />
                                            <div className="date">31 มีนาคม 2564</div>
                                            <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                            <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                        </Link>
                                    </div>
                                </li>
                                <li className="hex" data-aos="flip-left" data-aos-delay={300}>
                                    <div className="hexIn">
                                        <Link className="hexLink" to="/news-detail">
                                            <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-2.png)' }} />
                                            <div className="date">31 มีนาคม 2564</div>
                                            <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                            <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                        </Link>
                                    </div>
                                </li>
                                <li className="hex" data-aos="flip-left" data-aos-delay={300}>
                                    <div className="hexIn">
                                        <Link className="hexLink" to="/news-detail">
                                            <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-3.png)' }} />
                                            <div className="date">31 มีนาคม 2564</div>
                                            <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                            <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                        </Link>
                                    </div>
                                </li>
                                <li className="hex" data-aos="flip-left" data-aos-delay={300}>
                                    <div className="hexIn">
                                        <Link className="hexLink" to="/news-detail">
                                            <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-4.png)' }} />
                                            <div className="date">31 มีนาคม 2564</div>
                                            <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                            <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                        </Link>
                                    </div>
                                </li>
                                <li className="hex" data-aos="flip-left" data-aos-delay={300}>
                                    <div className="hexIn">
                                        <Link className="hexLink" to="/news-detail">
                                            <div className="img" style={{ backgroundImage: 'url(img/background/img-hex-5.png)' }} />
                                            <div className="date">31 มีนาคม 2564</div>
                                            <div className="title text-limit">กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</div>
                                            <div className="desc text-limit">วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</div>
                                        </Link>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div className="action text-center" data-aos="fade-up">
                            <Link to="/news" className="btn btn-secondary btn-round-5 btn-large">ดูทั้งหมด</Link>
                        </div>
                    </div>
                </div>
                {/* news */}
                {/* login */}
                <div className="wg-login">
                    <div className="container">
                        <div className="row no-gutters">
                            <div className="col-md-6 order-2 order-md-1" data-aos="fade-up-right">
                                <div className="cover">
                                    <div className="b-img-1">
                                        <img src="img/background/money-1.png" />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 order-1 order-md-2 height" data-aos="fade-left">
                                <div className="box-text">
                                    <div className="h-title text-light">ลงชื่อเข้าใช้</div>
                                    <div className="desc text-light">ลงชื่อเข้าใช้งาน DMCR SLIP กรมทรัพยากรทางทะเล<br />และชายฝั่ง กระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม</div>
                                    <div className="action">
                                        <Link to="/login" className="btn btn-primary btn-round-5 btn-large border-0">เข้าสู่ระบบ</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* login */}
                {/* about */}

                <SlickAbout_Home />

                {/* about */}
            </section>
        </React.Fragment>
    );
}

export default Home;