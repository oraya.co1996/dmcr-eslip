import React from 'react'
import { Link, Route, useHistory} from 'react-router-dom';
import SlickAlbum_News from '../slick/SlickAlbum_News'
import SlickDownload_News from '../slick/SlickDownload_News'
import SlickRelated_News from '../slick/SlickRelated_News'

function New_detail() {

    const history = useHistory();

    return (
        <React.Fragment>
            <section className="site-container">
                <div className="default-headcover" data-aos="fade-down" style={{ backgroundImage: 'url("img/background/tp-img1.png")' }}>
                    <div className="container">
                        <div className="row align-items-center height">
                            <div className="col">
                                <h1 className="h-title">ข่าวสาร &amp; กิจกรรม</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="breadcrumb-block">
                    <div className="container">
                        <ol className="breadcrumb" data-aos="fade-right">
                            <li>
                                <Link to="/" className="link">
                                    หน้าหลัก
                                </Link>
                            </li>
                            <li>
                                <a href="news.php" className="link">
                                    ข่าวสาร &amp; กิจกรรม
                                </a>
                            </li>
                            <li className="active">
                                กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ
                            </li>
                        </ol>
                        <a href="javascript:void(0);" onClick={() => history.goBack()} className="link btn-back" data-aos="fade-left">
                            <i data-feather="chevron-left" /> <span>กลับ</span>
                        </a>
                    </div>
                </div>
                <div className="default-innerpage">
                    <div data-aos="zoom-in-left" data-aos-delay={300}>
                        <div className="mn mn-lg"><img src="img/background/w810-lg.png" /></div>
                    </div>
                    <div data-aos="zoom-in-right">
                        <div className="mn mn-sm"><img src="img/background/w810-sm.png" /></div>
                    </div>
                    <div className="container">
                        <div className="detail-block">
                            <div className="header">
                                <div className="row align-items-center">
                                    <div className="col-sm">
                                        <h2 className="title" data-aos="fade-right">
                                            กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ
                                        </h2>
                                    </div>
                                    <div className="col-sm-auto">
                                        <div className="info" data-aos="fade-left">
                                            <ul className="item-list">
                                                <li>
                                                    <span className="feather icon-calendar" />
                                                    13 พ.ย. 2563
                                                </li>
                                                <li>
                                                    <span className="feather icon-eye" />
                                                    180
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="body">
                                <div className="editor-content" data-aos="fade-up">
                                    <img className="img" src="img/upload/about-pjsource.png" alt="" width="100%" />
                                    <br /><br />
                                    <p>31 มีนาคม 2564</p>
                                    <p>กรมทะเล ร่วมประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ</p>
                                    <p>วันที่ 31 มีนาคม 2564 นายวราวุธ ศิลปอาชา รัฐมนตรีว่าการกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม เป็นประธานการประชุมคณะอนุกรรมการมรดกโลกทางธรรมชาติ ครั้งที่ 1/2564</p>
                                </div>
                                <div className="vdo" data-aos="zoom-in">
                                    <iframe src="https://www.youtube.com/embed/GXsvOebAtFE" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen width={560} height={315} frameBorder={0} />
                                </div>
                                {/* Album */}

                                <SlickAlbum_News />

                                {/* Album */}

                                {/* Download */}

                                <SlickDownload_News />

                                {/* Download */}

                                <div className="botton-nav" data-aos="fade-up">
                                    <a className="btn btn-medium btn-secondary btn-round-5 -back" href="javascript:history.back()">
                                        <span className="feather icon-chevron-left" />
                                        กลับ
                                    </a>
                                    <a className="btn btn-medium btn-secondary btn-round-5 -next" href="#">
                                        ถัดไป
                                        <span className="feather icon-chevron-right" />
                                    </a>
                                </div>
                                <div className="share" data-aos="fade-up">
                                    <div className="icon-share-fb">
                                        <div className="fb-share-button fb_iframe_widget fb_iframe_widget_fluid" data-href="#">
                                            <span style={{ verticalAlign: 'bottom', width: '49px', height: '15px' }}>
                                                <iframe name="f1ec79f8030426" data-testid="#" title="fb:share_button Facebook Social Plugin" allowTransparency="true" allowFullScreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v3.0/plugins/share_button.php?app_id=&channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df33cfda56d7629c%26domain%3Dthailandcoralbleaching.dmcr.go.th%26origin%3Dhttps%253A%252F%252Fthailandcoralbleaching.dmcr.go.th%252Ff3b463fab32f204%26relation%3Dparent.parent&container_width=0&href=https%3A%2F%2Fthailandcoralbleaching.dmcr.go.th%2Fth%2Fblog_coral%2Fdetail%2F78&locale=en_US&sdk=joey" style={{ border: 'none', visibility: 'visible', width: '49px', height: '15px' }} className width="1000px" height="1000px" frameBorder={0} />
                                            </span>
                                        </div>
                                    </div>
                                    <div className="icon-share-tw">
                                        <a target="_blank" href="#">
                                            <img src="img/icon/icon-share-tw.png" alt="icon-share-tw" />
                                        </a>
                                    </div>
                                    <div className="icon-share-p">
                                        <a href="javascript:window.print();">
                                            <img src="img/icon/icon-share-p.png" alt="icon-share-p" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="detail-block">
                            <div className="header">
                                <div className="row align-items-center">
                                    <div className="col-sm">
                                        <h2 className="title ab-title" data-aos="fade-down">
                                            ข่าวสาร &amp; กิจกรรม ที่เกี่ยวข้อง
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            <div className="body">
                                {/* SlickRelated_News */}

                                <SlickRelated_News />

                                {/* SlickRelated_News/ */}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default New_detail
