import React from 'react'
import { Link, useHistory } from 'react-router-dom';

function News() {

  const history = useHistory();

  return (
    <React.Fragment>
      <section className="site-container">
        <div className="default-headcover" data-aos="fade-down" style={{ backgroundImage: 'url("./img/background/tp-img1.png")' }}>
          <div className="container">
            <div className="row align-items-center height">
              <div className="col">
                <h1 className="h-title">ข่าวสาร &amp; กิจกรรม</h1>
              </div>
            </div>
          </div>
        </div>
        <div className="breadcrumb-block">
          <div className="container">
            <ol className="breadcrumb" data-aos="fade-right">
              <li>
                <Link to="/" className="link">
                  หน้าหลัก
                </Link>
              </li>
              <li className="active">
                ข่าวสาร &amp; กิจกรรม
              </li>
            </ol>
            <a href="javascript:void(0)" onClick={() => history.goBack()} className="link btn-back" data-aos="fade-left">
              <i data-feather="chevron-left" /> <span>กลับ</span>
            </a>
          </div>
        </div>
        <div className="default-innerpage">
          <div data-aos="zoom-in-left" data-aos-delay={300}>
            <div className="mn mn-lg"><img src="img/background/w810-lg.png" /></div>
          </div>
          <div data-aos="zoom-in-right">
            <div className="mn mn-sm"><img src="img/background/w810-sm.png" /></div>
          </div>
          <div className="container">
            <div className="detail-block">
              <div className="header">
                <div className="row align-items-center">
                  <div className="col-sm">
                    <h2 className="title text-center" data-aos="fade-down">
                      ข่าวสาร &amp; กิจกรรม
                    </h2>
                  </div>
                </div>
              </div>
              <div className="body">
                <div className="news-list">
                  <ul className="item-list">
                    <li className="-hilight" data-aos="flip-left" data-aos-delay={300}>
                      <Link className="link" to="/news-detail">
                        <div className="hexGrid">
                          <div className="hex">
                            <div className="hexIn">
                              <div className="hexLink">
                                <div className="img" style={{ backgroundImage: 'url(img/background/pic.jpeg)' }} />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="title text-limit">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''''s standard dummy</div>
                        <div className="desc text-limit">ontrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece
                                             of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney 
                                             College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the
                                              cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 
                                              of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory
                                               of ethics, very popular during the Renaissance. The first line of Lor</div>
                        <div className="date-read">
                          <div className="date">
                            <span className="feather icon-calendar" />
                            23-03-2021
                          </div>
                          <div className="read">
                            <div className="btn btn-medium btn-round-5 btn-primary">อ่านต่อ</div>
                          </div>
                        </div>
                      </Link>
                    </li>
                    <li className="-hilight" data-aos="flip-left" data-aos-delay={300}>
                      <Link className="link" to="news-detail">
                        <div className="hexGrid">
                          <div className="hex">
                            <div className="hexIn">
                              <div className="hexLink">
                                <div className="img" style={{ backgroundImage: 'url(img/background/57174.jpg)' }} />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="title text-limit">Test</div>
                        <div className="desc text-limit">Test</div>
                        <div className="date-read">
                          <div className="date">
                            <span className="feather icon-calendar" />
                            20-04-2021
                          </div>
                          <div className="read">
                            <div className="btn btn-medium btn-round-5 btn-primary">อ่านต่อ</div>
                          </div>
                        </div>
                      </Link>
                    </li>

                  </ul>
                </div>
                <div className="pagination-block" data-aos="fade-up">
                  <div className="row row-0 align-items-center">
                    <div className="col">
                      <div className="pagination-label">
                        <div className="title">
                          จำนวนทั้งหมด <span>2</span> รายการ
                        </div>
                      </div>
                    </div>
                    <div className="col-auto">
                      <div className="pagination">
                        <ul className="item-list">
                          <li className="pagination-nav"><a className="link" href="#"><span className="feather icon-chevrons-left" /></a></li>
                          <li className="pagination-nav"><a className="link" href="#"><span className="feather icon-chevron-left" /></a></li>
                          <li className="active"><a className="link" href="#">01</a></li>
                          {/* <li><a className="link" href="#">02</a></li>
                          <li><a className="link" href="#">03</a></li> */}
                          {/* <li className="jumpPage">
                            <div className="select-box">
                              <select className="select-control" size={1} style={{ width: '100%' }}>
                                <option value={1} selected>ไปหน้า</option>
                                <option value={2}>....</option>
                                <option value={3}>....</option>
                              </select>
                            </div>
                          </li> */}
                          <li className="pagination-nav"><a className="link" href="#"><span className="feather icon-chevron-right" /></a></li>
                          <li className="pagination-nav"><a className="link" href="#"><span className="feather icon-chevrons-right" /></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </React.Fragment>
  )
}

export default News



