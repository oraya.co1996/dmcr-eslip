import React from 'react'
import { Link, Route, useHistory } from 'react-router-dom';
import SlickDownload_About from '../slick/SlickDownload_About';

function Otp() {

    const history = useHistory();

    return (
        <React.Fragment>
            <section className="site-container">
                <div className="default-headcover" data-aos="fade-down" style={{ backgroundImage: 'url("img/background/tp-img1.png")' }}>
                    <div className="container">
                        <div className="row align-items-center height">
                            <div className="col">
                                <h1 className="h-title">ที่มาโครงการ</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="breadcrumb-block">
                    <div className="container">
                        <ol className="breadcrumb" data-aos="fade-right">
                            <li>
                                <Link to="/" className="link">
                                    หน้าหลัก
                                </Link>
                            </li>
                            <li className="active">
                                เข้าสู่ระบบ
                            </li>
                        </ol>
                        <a href="javascript:void();" onClick={() => history.goBack()} className="link btn-back" data-aos="fade-left">
                            <i data-feather="chevron-left" /> <span>กลับ</span>
                        </a>
                    </div>
                </div>
                <div className="breadcrumb-block">
                    <div className="container">
                        <ol className="breadcrumb" data-aos="fade-right">
                            
                        <fieldset>
                            
                            <h3 class="h3">Enter the confirmation code below</h3>
                            <input type="text" name="twitter" placeholder="Enter confirmation code" />
                            <Link to="/login" type="button" name="go back" class="next action-button  " >Back</Link>
                            <input type="button" name="Submit" class="next action-button" value="Submit" />

                        </fieldset>


                        </ol>

                    </div>
                </div>

            </section>
        </React.Fragment>
    )
}

export default Otp
