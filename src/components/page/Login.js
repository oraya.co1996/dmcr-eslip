import React from 'react'
import { Link, Route, useHistory } from 'react-router-dom';
import SlickDownload_About from '../slick/SlickDownload_About';
import axios from 'axios';


function Login() {

const history = useHistory();

var axios = require('axios');
var data = JSON.stringify({"apptoken":"dmcr-eslip-2564-api"});

var config = {
  method: 'get',
  url: 'http://api.dmcr.go.th:3500/service-api/home/api_mobile?method=new_allareas_sum&countyID=65',
  headers: { 
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJbmZvIjp7ImFwcF90b2tlbiI6ImRtY3ItYmlnZGF0YS0yNTY0LWFwaSJ9LCJpYXQiOjE2MjE1MjU4MzIsImV4cCI6MTYyMjEzMDYzMn0.2U-H-BPRHfqyHd2cDPmraBlo-t7Q04I9A80fwFd6b7Q', 
    'Content-Type': 'application/json'
  },
  data : data
};

axios(config)
.then(function (response) {
  console.log(JSON.stringify(response.data));
})
.catch(function (error) {
  console.log(error);
});
    return (
        <React.Fragment>
            <section className="site-container">
                <div className="default-headcover" data-aos="fade-down" style={{ backgroundImage: 'url("img/background/tp-img1.png")' }}>
                    <div className="container">
                        <div className="row align-items-center height">
                            <div className="col">
                                <h1 className="h-title">ที่มาโครงการ</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="breadcrumb-block">
                    <div className="container">
                        <ol className="breadcrumb" data-aos="fade-right">
                            <li>
                                <Link to="/" className="link">
                                    หน้าหลัก
                                </Link>
                            </li>
                            <li className="active">
                                เข้าสู่ระบบ
                            </li>
                        </ol>
                        <a href="javascript:void();" onClick={() => history.goBack()} className="link btn-back" data-aos="fade-left">
                            <i data-feather="chevron-left" /> <span>กลับ</span>
                        </a>
                    </div>
                </div>
                <div className="breadcrumb-block">
                    <div className="container">
                        <ol className="breadcrumb" data-aos="fade-right">
                            
                            <fieldset>
        
                                <h3 class="h3">Enter your Id Card </h3>


                                <input type="text" name="cpass" placeholder="Enter IdCard" />

                                <div className="action">
                                        <Link to="/otp" className="btn btn-primary btn-round-5 btn-large border-0">Sent</Link>
                                    </div>

                            </fieldset>
                            

                        </ol>

                    </div>
                </div>

            </section>
        </React.Fragment>
    )
}

export default Login
