import React from 'react'
import { Link, Route, useHistory } from 'react-router-dom';
import SlickDownload_About from '../slick/SlickDownload_About';

function About() {

    const history = useHistory();

    return (
        <React.Fragment>
            <section className="site-container">
                <div className="default-headcover" data-aos="fade-down" style={{ backgroundImage: 'url("img/background/tp-img1.png")' }}>
                    <div className="container">
                        <div className="row align-items-center height">
                            <div className="col">
                                <h1 className="h-title">ที่มาโครงการ</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="breadcrumb-block">
                    <div className="container">
                        <ol className="breadcrumb" data-aos="fade-right">
                            <li>
                                <Link to="/" className="link">
                                    หน้าหลัก
                                </Link>
                            </li>
                            <li className="active">
                                ที่มาโครงการ
                            </li>
                        </ol>
                        <a href="javascript:void();" onClick={() => history.goBack()} className="link btn-back" data-aos="fade-left">
                            <i data-feather="chevron-left" /> <span>กลับ</span>
                        </a>
                    </div>
                </div>
                <div className="default-innerpage">
                    <div data-aos="zoom-in-left" data-aos-delay={300}>
                        <div className="mn mn-lg"><img src="img/background/w810-lg.png" /></div>
                    </div>
                    <div data-aos="zoom-in-right">
                        <div className="mn mn-sm"><img src="img/background/w810-sm.png" /></div>
                    </div>
                    <div className="container">
                        <div className="detail-block">
                            <div className="header">
                                <div className="row align-items-center">
                                    <div className="col-sm">
                                        <h2 className="title" data-aos="fade-right">
                                            ที่มาโครงการ
                                        </h2>
                                    </div>
                                    <div className="col-sm-auto">
                                        <div className="info" data-aos="fade-left">
                                            <ul className="item-list">
                                                <li>
                                                    <span className="feather icon-calendar" />
                                                    13 พ.ย. 2563
                                                </li>
                                                <li>
                                                    <span className="feather icon-eye" />
                                                    180
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="body">
                                <div className="editor-content" data-aos="fade-up">
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="ab-topgraphic">
                                                <div className="cover">
                                                    <img className="img" src="img/upload/about-pjsource.png" alt="ที่มาโครงการ" width="100%" />
                                                </div>
                                                <div className="text-title"><span>ที่มาโครงการ</span><br />โครงการบริหารจัดการศูนย์ข้อมูลกลางด้านทรัพยากรทางทะเลและชายฝั่งรายจังหวัด</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row row-40 pt-4">
                                        <div className="col-lg-6 order-2 order-lg-1 pt-lg-0 pt-4">
                                            <div className="cover text-center">
                                                <img src="img/upload/about-img1.png" width="100%" />
                                            </div>
                                        </div>
                                        <div className="col-lg-6 order-1 order-lg-2 d-flex align-items-center">
                                            <div>
                                                <div className="about-card">
                                                    <div className="circle-icon bg-blue">
                                                        <img src="img/upload/about-target.png" />
                                                    </div>
                                                    <div className="about-body">
                                                        <div>
                                                            <div className="title">เป้าหมาย</div>
                                                            <div className="desc">
                                                                มีการกำหนดเขตทรัพยากรทางทะเลและชายฝั่ง
                                                                และจัดทำ One Marine Chart
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="about-card pt-3 pt-lg-4">
                                                    <div className="circle-icon bg-green">
                                                        <img src="img/upload/about-goal.png" />
                                                    </div>
                                                    <div className="about-body">
                                                        <div>
                                                            <div className="title">ค่าเป้าหมายที่ต้องบรรลุภายในปี 2565</div>
                                                            <div className="desc">
                                                                ต้นฉบับแผนที่การจำแนกเขตทางทะเลและชายฝั่งรายจังหวัด
                                                                กลุ่มจังหวัด และภาพรวมของพื้นที่ทางทะเล
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row row-40 pt-4">
                                        <div className="col-lg-6">
                                            <div className="ab-title">
                                                <div className="ab-img"><img src="img/upload/about-coconut.png" /></div>
                                                <div className="title">
                                                    สถานการณ์ในภาพรวมของเรื่อง<br />
                                                    และประเด็นปฏิรูป
                                                </div>
                                            </div>
                                            <div className="ab-desc">
                                                ประเทศไทยมีจังหวัดที่มีขอบเขตติตทะเล 23 จังหวัด แบ่งเป็นทะเลฝั่งตะวันตก ได้แก่ ทะลอันดามัน รวมไปถึงช่องแคบมะละกา 6 จังหวัด และทะเลฝั่งตะวันออก ได้แก่ ทะเลฝั่งอ่าวไทย 17 จังหวัด ซึ่งปัจจุบันประเทศไทยมีจังหวัดที่มีการกำหนดเขตจังหวัดในการบริหารทรัพยากรจากการแบ่งเขตทางทะเลได้อย่างชัดเจนเพียง 7 จังหวัด ได้แก่ จังหวัดเพชรบุรี จังหวัดสมุทรสงคราม จังหวัดสมุทรสาคร จังหวัดสมุทรปราการ จังหวัดฉะเชิงเทรา จังหวัดชลบุรี และกรุงเทพมหานคร ซึ่งเป็นจังหวัดในเขตอ่าวไทยตอนใน ทั้งนี้ อีก 16 จังหวัดที่มีขอบเขดติดทะเลยังไม่มีแผนที่การใช้ประโยชน์ทางทะเลที่ชัดเจน ส่งผลให้ภาพรวมการใช้ประโยชน์และการแบ่งเขตทางทางทะเลของประเทศไทยยังขาดความชัดเจน ดังนั้น การจัดทำแผนที่การใช้ประโยชน์ทางทะเล และการวางแผนเชิงพื้นที่ทางทะเลของไทย จึงมีความจำเป็นที่จะต้องเร่งดำเนินการจัดทำแผนที่ภาพรวมของประเทศและแผนที่เฉพาะที่เป็นมาตรฐานเดียวกัน เพื่อให้หน่วยงานที่เกี่ยวข้องนำไปใช้ในภารกิจที่รับผิดชอบต่อไป และเพื่อการรักษาผลประโยชน์ของชาติทางทะเล โดยการสร้างความสมดุลของทรัพยากรและสิ่งแวดล้อมทางทะเลให้เป็นฐานที่มั่นคงในการพัฒนาด้านเศรษฐกิจของไทยให้มีความมั่นคง มั่งคั่ง และยั่งยืน ต่อไปในอนาคต
                                            </div>
                                        </div>
                                        <div className="col-lg-6 pt-lg-0 pt-4">
                                            <div className="cover text-center">
                                                <img src="img/upload/about-img2.png" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row pt-4">
                                        <div className="col-12">
                                            <div className="ab-title">
                                                <div className="ab-img"><img src="img/upload/about-boat.png" /></div>
                                                <div className="title">
                                                    การดำเนินงานที่ผ่านมา
                                                </div>
                                            </div>
                                            <div className="ab-desc">
                                                หน่วยงานภาครัฐที่เกี่ยวข้องได้ร่วมกันผลักดันการกำหนดเขตจังหวัดทางทะเลของประเทศให้มีมาตรฐานเตียวกัน โดยกระทรวงมหาดไทยได้ประชุมหารือกับหน่วยงานและจังหวัดที่เกี่ยวข้องอย่างต่อเนื่องจนได้ข้อยุติในการตกลงเรื่องเขตจังหวัดทางทะเลของทั้ง 23 จังหวัดแล้ว ซึ่งอยู่ในระหว่างการทำบันทึกข้อตกลงร่วมกันของจังหวัดต่างๆ ทั้ง 23 จังหวัด เพื่อที่จะนำรายละเอียดของข้อตกลงดังกล่าวนั้นมาดำเนินการจัดทำกฎหมายในการแบ่งเขตทางทะเลของประเทศไทยรายจังหวัด คือ <span className="text-highlight">ร่างพระราชบัญญัติการกำหนดและปรับปรุงพื้นที่เขตการปกครองของจังหวัดทางทะเล พ.ศ. ....</span> ซึ่งจะก่อให้เกิดความชัดเจนของเขตการปกครองของจังหวัดทางทะเลทั้ง 23 จังหวัดชายทะเลต่อไป
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* Download */}

                                <SlickDownload_About/>
                                
                                {/* Download */}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default About
