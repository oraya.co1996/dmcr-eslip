import React from 'react';
import { Link } from 'react-router-dom';

function Headermember() {
    return (
        <React.Fragment>
<header class="site-header -site-member -site-member-page">
	<div class="main-header">
		<div class="container">
			<div class="row no-gutters align-items-center">
				<div class="col">
					<div class="brand" data-aos="fade-right">
						<Link to="/" title="กรมทรัพยากรทางทะเลและชายฝั่ง Department of Marine and Coastal Resources, Thailand">
							<img src="img//static/brand.png" title="กรมทรัพยากรทางทะเลและชายฝั่ง Department of Marine and Coastal Resources, Thailand" alt="กรมทรัพยากรทางทะเลและชายฝั่ง Department of Marine and Coastal Resources, Thailand" />
						</Link>
						<div class="b-brand" data-aos="fade-right" data-aos-delay="300">
							<Link to="/" class="brand-txt link" title="กรมทรัพยากรทางทะเลและชายฝั่ง Department of Marine and Coastal Resources, Thailand">
								<span>ระบบใบแจ้งเงินเดือน ค่าจ้างประจำ และค่าตอบแทนพนักงานราชการ (E-Slip)</span><br />
								กรมทรัพยากรทางทะเลและชายฝั่ง
							</Link>
						</div>
					</div>
				</div>
				<div class="col-auto">
					<a href="dashboard.php" class="link">
						<div class="member" data-aos="fade-left" data-aos-delay="300">
							<div class="thumb">
								<figure class="cover">
									<img src="img/upload/user.jpg" alt="ข้าราชการ ลินดา สุทธิกาณจน์" class="lazy" />
								</figure>
								<div class="ojb-shadow"></div>
							</div>
							<div class="txt">
								ข้าราชการ ลินดา สุทธิกาณจน์
								ข้าราชการ ลินดา สุทธิกาณจน์
							</div>
						</div>
					</a>
				</div>
				<div class="col-auto">
					<div class="action" data-aos="fade-left">
						<a href="index.php" class="btn btn-logout">
							<img src="img/icon/i-logout.svg" alt="logout" />
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
        </React.Fragment>
    );
}

export default Headermember;