
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
// import Header from './components/Header';
import Body from './components/Body';
// import Footer from './components/Footer';
import AOS from 'aos';
import 'aos/dist/aos.css'; // You can also use <link> for styles
import Login from './components/Login';
import CheckIdNo from './components/CheckIdNo';
import Home from './components/Home';
import Profile from './components/Profile';
import EditProfile from './components/EditProfile';
import OTP from './components/OTP';
import News from './components/page/News';
import About from './components/page/About';

function App() {
  var diff =(new Date().getTime() - parseInt(localStorage.getItem("time"))) / 1000;
  diff /= 60;
  if(Math.abs(Math.round(diff)) >= 20) {
    localStorage.removeItem("time");
  }
  return (
    <Router>
      <div>
        <Switch>
          <Route exact path='/' component={Body} />
          <Route path='/check-idno' component={CheckIdNo} />
          <Route path='/otp' component={OTP} />
          <Route path='/login' component={Login} />
          <Route path='/home' component={Home} />
          <Route path='/profile' component={Profile} />
          <Route path='/edit-profile' component={EditProfile} />
          <Route exact path="/"><Home /></Route>
          <Route path="/news"><News /></Route>
          <Route path="/about"><About /></Route>   
          <Route path="/:id">          
            <p>Page Not Found.</p>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

AOS.init();
export default App;
