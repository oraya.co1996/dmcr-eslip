$(document).ready(function () {
    $('.wg-about-list .slider').slick({
        prevArrow:"<div class='slick-prev'><img src='assets/img/icon/arrows-left.svg'></div>",
        nextArrow:"<div class='slick-next'><img src='assets/img/icon/arrows-right.svg'></div>",
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        autoplay: false,
        autoplaySpeed: 3000,
        speed: 1000,
        responsive: [
            // {
            //     breakpoint: 575,
            //     settings: {
            //         rows: 1,
            //         slidesPerRow:1
            //     }
            // },
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    speed: 600,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    arrows: false,
                    speed: 600
                }
            }
        ]
    });
});