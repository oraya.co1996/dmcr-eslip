window.onload = function() {
    $(".preload").delay(400).fadeOut("200", function () {
        $('#preload').addClass('move');
        $('#preload').fadeOut(200);
    });
};

AOS.init({
    once: true,
    duration: 2000,
    offset: -50,
    easing: 'ease',
});

$(document).ready(function () {
    new WOW().init();

    $('.item-matchHeight').matchHeight();

    $('.select-control').select2({
        minimumResultsForSearch: Infinity,
        placeholder: "Select"
    });
    
    $('.select-control.has-search').select2({
        placeholder: "Select"
    });

    $("[data-fancybox]").fancybox({
        thumbs     : false,
        slideShow  : false,
        fullScreen : false
    });

    $(".mcscroll").mCustomScrollbar({
       axis : "y",
       scrollButtons: {
           enable: true
       }
    });
    $(".mcscrollX").mCustomScrollbar({
       axis : "x",
       scrollButtons: {
           enable: true
       }
    });

    var lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy"
    });

    // $('.overflow-line-1').trunk8({
    //    lines: 1,
    //    tooltip : false
    // });
    // $('.overflow-line-2').trunk8({
    //    lines: 2,
    //    tooltip : false
    // });
    // $('.overflow-line-3').trunk8({
    //    lines: 3,
    //    tooltip : false
    // });

    var topbar = $('.site-header').height();
    $(window).scroll(function() {
        if ($(window).scrollTop() > topbar) {
            $(".site-header").addClass("tiny");
        } else {
            $(".site-header").removeClass("tiny");
        }
    });

    $('[data-toggle="menu-mobile"]').click(function(){
        $(this).toggleClass('close');
        $('.global-container').toggleClass('sidebar-open');
        $('nav.menu').toggleClass('open');
    });
    $('[data-toggle="menu-overlay"]').click(function(){
        $('[data-toggle="menu-mobile"]').removeClass('close');
        $('.global-container').removeClass('sidebar-open');
        $('nav.menu').removeClass('open');
    });

    // $('.main-slider').slick({
    //     prevArrow:"<div class='slick-prev'><span class='feather icon-chevron-left'></span></div>",
    //     nextArrow:"<div class='slick-next'><span class='feather icon-chevron-right'></span></div>",
    //     infinite: true,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     dots: true,
    //     arrows: true,
    //     focusOnSelect: true,
    //     rows: 1,
    //     slidesPerRow:1,
    //     responsive: [
    //         {
    //             breakpoint: 575,
    //             settings: {
    //                 rows: 1,
    //                 slidesPerRow:1
    //             }
    //         },
    //         {
    //             breakpoint: 767,
    //             settings: {
    //                 rows: 1,
    //                 slidesPerRow:1
    //             }
    //         },
    //         {
    //             breakpoint: 1366,
    //             settings: {
    //                 rows: 1,
    //                 slidesPerRow:1
    //             }
    //         }
    //     ]
    // });
    
    // $('.btn-mobile').click(function() {
    //     $('.nav-menu').toggleClass('open');
    //     $(this).toggleClass('close');
    // });

    $('.gallery').slick({
        dots: true,
        infinite: false,
        arrows: false,
        speed: 1500,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    });

    $('.download-list .slider').slick({
        dots: true,
        infinite: false,
        arrows: false,
        speed: 1500,
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    speed: 300
                }
            },
        ]
    });

    $('.other-news').slick({
        dots: true,
        infinite: false,
        arrows: false,
        speed: 1500,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.bg-slide').slick({
        dots: false,
        infinite: true,
        arrows: false,
        speed: 3000,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    autoplaySpeed: 2000
                }
            },
        ]
    });

});