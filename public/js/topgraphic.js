$(document).ready(function () {
    $('.topgraphic .slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1000,
        rows: 1,
        slidesPerRow:1,
        fade: true,
        cssEase: 'linear',
        responsive: [
            {
                breakpoint: 575,
                settings: {
                    rows: 1,
                    slidesPerRow:1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    rows: 1,
                    slidesPerRow:1
                }
            },
            {
                breakpoint: 1366,
                settings: {
                    rows: 1,
                    slidesPerRow:1
                }
            }
        ]
    });
});